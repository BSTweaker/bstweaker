#!/bin/sh

install_dir="${HOME}/Library/Application Support/BathyScaphe/PlugIns"
install_name="${install_dir}/BSTweaker.plugin"
tmp_dir=$(mktemp -d /tmp/bstweaker_install.XXXXXX)

function cleanup_and_exit() {
	cd /tmp
	rm -rf "$tmp_dir"
	exit
}

cd "$tmp_dir"

file=$(curl -L -s -S https://bitbucket.org/BSTweaker/bstweaker/downloads/ | grep 'BSTweaker-.*\.zip' | awk -F '"' '{print $2}' | head -n 1)
if [ -z "$file" ]; then
	printf "\033[31m%s\033[m\n" "Error: Cannot find a plugin URL."
	cleanup_and_exit
fi

url="https://bitbucket.org${file}"
echo "Downloading a plugin from ${url}..."

curl -O -L -s -S "$url"
if [ $? -ne 0 ]; then
	printf "\033[31m%s\033[m\n" "Error: Cannot download the plugin."
	cleanup_and_exit
fi

read -p "Do you also want to install a supplemental plugin? This might be useful for BathyScaphe 3.1 and earlier. (y/N): " yn

install_suppl=0
supplfile=""

while :
do
	case "$yn" in
		[nN]*) break;;
		[yY]*) install_suppl=1; break;;
		*) if [ -z "$yn" ]; then break; fi;;
	esac
	read -p "Y/n: " yn
done

if [ $install_suppl -ne 0 ]; then
	supplfile=$(curl -L -s -S https://bitbucket.org/BSTweaker/bstweaker/downloads/ | grep 'BSTweakerSuppl-.*\.zip' | awk -F '"' '{print $2}' | head -n 1)
	if [ -z "$supplfile" ]; then
		supplfile=""
		printf "\033[31m%s\033[m\n" "Error: Cannot find the supplemental plugin URL."
	else
		url="https://bitbucket.org${supplfile}"
		echo "Downloading a supplemental plugin from ${url}..."
		curl -O -L -s -S "$url"
		if [ $? -ne 0 ]; then
			printf "\033[31m%s\033[m\n" "Error: Cannot download the supplemental plugin."
			supplfile=""
		fi
	fi
fi

read -p "The plugin has been downloaded as ${tmp_dir}/$(basename $file). Install it? (Y/n): " yn

while :
do
	case "$yn" in
		[nN]*) open .; exit;;
		[yY]*) break;;
		*) if [ -z "$yn" ]; then break; fi;;
	esac
	read -p "Y/n: " yn
done

if [ -d "$install_name" ]; then
	read -p "An old plugin is already installed. Are you sure you want to overwrite it? (Y/n): " yn

	while :
	do
		case "$yn" in
			[nN]*) open .; exit;;
			[yY]*) break;;
			*) if [ -z "$yn" ]; then break; fi;;
		esac
		read -p "Y/n: " yn
	done
	cp -a "$install_name" "$tmp_dir"
	rm -r "$install_name"
	if [ $? -ne 0 ]; then
		printf "\033[31m%s\033[m\n" "Error: Cannot remove the old plugin."
		cleanup_and_exit
	fi
fi

mkdir -p "$install_dir"
unzip "${tmp_dir}/$(basename $file)" -d "$install_dir"

if [ $? -eq 0 ]; then
	if [ -n "$supplfile" ]; then
		unzip -o "${tmp_dir}/$(basename $supplfile)" -d "$install_dir"
	fi
	echo "Installed the plugin to ${install_name} successfully. Restart BathyScaphe if running."
else
	printf "\033[31m%s\033[m\n" "Error: Installation failed."
	if [ -d "$install_name" ]; then
		rm -rf "$install_name"
	fi
	if [ -d "${tmp_dir}/BSTweaker.plugin" ]; then
		cp -a "${tmp_dir}/BSTweaker.plugin" "$install_dir"
	fi
fi

cleanup_and_exit
