//
//  BSTYenToBackslashConverter.m
//  BSTweaker
//
//  Created by anonymous on 2018/12/25.
//  Copyright © 2018年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

static uint64_t backslashCompatibilityMask;

@interface BSTYenToBackslashConverter ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, readonly) float bundleVersion;
@end

@implementation BSTYenToBackslashConverter

+ (id)sharedInstance
{
    static BSTYenToBackslashConverter* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTYenToBackslashConverter alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        _bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([defaults integerForKey:@"BSTShowBackslashTransparently"] == NSOnState) backslashCompatibilityMask = ~1llu;
        else backslashCompatibilityMask = ~0llu;
    }
    return self;
}

- (void)swizzle
{
    Class ThreadView = objc_lookUpClass("CMRThreadView");
    Class ReplyMessenger = objc_lookUpClass("CMRReplyMessenger");
    Class AppDefaults = objc_lookUpClass("AppDefaults");
    if(!ThreadView || !ReplyMessenger) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    BOOL (^writeSelectionToPasteboard_mod)(id, NSPasteboard *, NSString *) = ^(id self, NSPasteboard *pboard, NSString *type) {
        BOOL ret = ((BOOL (*)(id, SEL, NSPasteboard *, NSString *))objc_msgSend)(self, @selector(mod_writeSelectionToPasteboard:type:), pboard, type);
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([type isEqualToString:NSStringPboardType] && [defaults boolForKey:@"BSTConvertYenToBackslashWhenCopy"]) {
            NSString *replaced = [[pboard stringForType:type] stringByReplacingOccurrencesOfString:@"\u00a5" withString:@"\\"];
            return [pboard setString:replaced forType:type];
        }
        return ret;
    };
    SWIZZLE_METHOD(ThreadView, writeSelectionToPasteboard:type:, writeSelectionToPasteboard_mod, "c@:@:@");
    
    NSString *(^preparedStringForPost_mod)(id, NSString *) = ^(id self, NSString *str) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSInteger backslashPostingMode = [defaults integerForKey:@"BSTBackslashPostingMode"];
        if(backslashPostingMode == 0) {
            return ((NSString* (*)(id, SEL, NSString *))objc_msgSend)(self, @selector(mod_preparedStringForPost:), str);
        } else {
            Class BoardManager = objc_lookUpClass("BoardManager");
            id manager = ((id (*)(id, SEL))objc_msgSend)(BoardManager, @selector(defaultManager));
            NSString *board = ((id (*)(id, SEL))objc_msgSend)(self, @selector(boardName));
            SEL newSelector = @selector(numericCharRefAvailablityMaskAtBoard:);
            SEL oldSelector = @selector(allowsCharRefAtBoard:);
            BOOL isNCRAvailable = NO;
            if([manager respondsToSelector:newSelector]) {
                uint64_t mask = ((uint64_t (*)(id, SEL, NSString*))objc_msgSend)(manager, newSelector, board);
                if(mask & 0x1) isNCRAvailable = YES;
            }
            else if([manager respondsToSelector:newSelector]) {
                isNCRAvailable = ((BOOL (*)(id, SEL, NSString*))objc_msgSend)(manager, oldSelector, board);
            }
            if(isNCRAvailable) {
                if(backslashPostingMode == 2) {
                    str = [str stringByReplacingOccurrencesOfString:@"\\" withString:@"&#92;"];
                }
                return [str stringByReplacingOccurrencesOfString:@"\u00a5" withString:@"&#165;"];
            }
            return [str stringByReplacingOccurrencesOfString:@"\u00a5" withString:@"\\"];
        }
    };
    SWIZZLE_METHOD(ReplyMessenger, preparedStringForPost:, preparedStringForPost_mod, "@@:@");
    
    if(_bundleVersion >= 1012) {
        uint64_t (^backslashCompatibilityMask_mod)(id) = ^(id self) {
            uint64_t mask = ((uint64_t (*)(id, SEL))objc_msgSend)(self, @selector(mod_backslashCompatibilityMask));
            return mask & backslashCompatibilityMask;
        };
        SWIZZLE_METHOD(AppDefaults, backslashCompatibilityMask, backslashCompatibilityMask_mod, "Q@");
        [[NSUserDefaults standardUserDefaults] addObserver:self
                                                forKeyPath:@"BSTShowBackslashTransparently"
                                                   options:NSKeyValueObservingOptionNew
                                                   context:(__bridge void *)self];
    }
    if(_bundleVersion >= 1096) {
        NSString *(^replyMessage_mod)(id) = ^(id self) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSInteger backslashPostingMode = [defaults integerForKey:@"BSTBackslashPostingMode"];
            NSString *str = ((NSString* (*)(id, SEL))objc_msgSend)(self, @selector(mod_replyMessage));
            if(backslashPostingMode == 0) return str;
            Class BoardManager = objc_lookUpClass("BoardManager");
            id manager = ((id (*)(id, SEL))objc_msgSend)(BoardManager, @selector(defaultManager));
            NSString *board = ((id (*)(id, SEL))objc_msgSend)(self, @selector(boardName));
            SEL newSelector = @selector(numericCharRefAvailablityMaskAtBoard:);
            SEL oldSelector = @selector(allowsCharRefAtBoard:);
            BOOL isNCRAvailable = NO;
            if([manager respondsToSelector:newSelector]) {
                uint64_t mask = ((uint64_t (*)(id, SEL, NSString*))objc_msgSend)(manager, newSelector, board);
                if(mask & 0x1) isNCRAvailable = YES;
            }
            else if([manager respondsToSelector:newSelector]) {
                isNCRAvailable = ((BOOL (*)(id, SEL, NSString*))objc_msgSend)(manager, oldSelector, board);
            }
            if(isNCRAvailable) {
                if(backslashPostingMode == 2) {
                    str = [str stringByReplacingOccurrencesOfString:@"\\" withString:@"&#92;"];
                }
                return [str stringByReplacingOccurrencesOfString:@"\u00a5" withString:@"&#165;"];
            }
            return [str stringByReplacingOccurrencesOfString:@"\u00a5" withString:@"\\"];
        };
        ADD_METHOD(ReplyMessenger, mod_replyMessage, replyMessage_mod, "@@");
    }
    
    self.status = BSTPluginStatusEnabled;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if(context == (__bridge void *)self && [keyPath isEqualToString:@"BSTShowBackslashTransparently"]) {
        if([change[@"new"] integerValue] == NSOnState) backslashCompatibilityMask = ~1llu;
        else backslashCompatibilityMask = ~0llu;
    }
    else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

@end
