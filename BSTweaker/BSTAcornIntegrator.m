//
//  BSTAcornIntegrator.m
//  BSTweaker
//
//  Created by anonymous on 2024/05/05.
//  Copyright © 2024年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import <Security/Security.h>
#import "BSTPlugIns.h"

static const NSString * const BSTKeychainServiceName = @"net.5ch.donguri.BSTweaker.login";

@interface BSTAcornIntegrator ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, readonly) float bundleVersion;
@property(nonatomic, strong) NSWindow *acornStatusWindow;
@property(nonatomic, weak) WebView *acornStatusWebView;
@property(nonatomic, readonly) BOOL prefer2chDomain;
@property(nonatomic, copy) NSString *cannonTargetURL;
@property(nonatomic, copy) NSString *cannonTargetDate;
@property(nonatomic, copy) NSString *userScriptPath;
@property() time_t lastLoginTrial;
@property() int loginRetry;
@property(weak) id loginObserver;
@end

@interface NSWorkspace () // implemented in SGAppKit
- (BOOL)openURL:(id)arg1 inBackground:(BOOL)arg2;
@end

static void modifyContextMenuForThreadView(NSMenu *menu)
{
    static NSString* const itemTitle = @"キャノン発射...";
    if ([menu itemWithTitle:itemTitle]) return;
    NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:itemTitle action:@selector(fireCannon:) keyEquivalent:@""];
    item.tag = -1;
    [menu addItem:item];
}

static NSString *encodeURL(NSString *str)
{
    return (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (__bridge CFStringRef)str, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8);
}

@interface BSTConnectionHandler : NSObject
@property (nonatomic, strong) NSArray<NSHTTPCookie *> *cookies;
#if !OS_OBJECT_USE_OBJC
@property (nonatomic, assign) dispatch_semaphore_t semaphore;
#else
@property (nonatomic, strong) dispatch_semaphore_t semaphore;
#endif
@property (nonatomic, strong) NSURLResponse *response;
@property (nonatomic, strong) NSMutableData *data;
@property (nonatomic, strong) NSError *error;
@end

@implementation BSTConnectionHandler
- (id)init
{
    self = [super init];
    if (self) {
        _semaphore = dispatch_semaphore_create(0);
    }
    return self;
}
- (void)dealloc
{
#if !OS_OBJECT_USE_OBJC
    dispatch_release(_semaphore);
#endif
}
- (void)connection:(NSURLConnection *)connection
didReceiveResponse:(NSURLResponse *)response
{
    self.response = response;
    self.cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:[(NSHTTPURLResponse *)response allHeaderFields] forURL:response.URL];
}
- (NSURLRequest *)connection:(NSURLConnection *)connection
             willSendRequest:(NSURLRequest *)request
            redirectResponse:(NSURLResponse *)response
{
    if (response) {
        return nil;
    }
    return request;
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(nonnull NSData *)data
{
    if (!_data) self.data = [NSMutableData data];
    [_data appendData:data];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    dispatch_semaphore_signal(_semaphore);
}
- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    self.error = error;
    dispatch_semaphore_signal(_semaphore);
}
@end

@implementation BSTAcornIntegrator

+ (id)sharedInstance
{
    static BSTAcornIntegrator* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTAcornIntegrator alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        float bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        _bundleVersion = bundleVersion;
        _acornStatusWindow = [[NSWindow alloc] initWithContentRect:NSMakeRect(0, 0, 800, 600) styleMask:NSWindowStyleMaskTitled|NSWindowStyleMaskClosable|NSWindowStyleMaskResizable|NSWindowStyleMaskMiniaturizable backing:NSBackingStoreBuffered defer:NO];
        [_acornStatusWindow center];
        _acornStatusWindow.title = @"どんぐり基地";
        _acornStatusWindow.frameAutosaveName = @"BSTAcornStatusWindow";
        _acornStatusWindow.delegate = self;
        _acornStatusWindow.releasedWhenClosed = NO;
        _userScriptPath = [[[[[NSBundle bundleForClass:[BSTAcornIntegrator class]] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"acorn_base.js"];
    }
    return self;
}

- (void)swizzle
{
    if (_status != BSTPluginStatusDisabled) return;
    
    Class ThreadViewer = objc_lookUpClass("CMRThreadViewer");
    Class ThreadView = objc_lookUpClass("CMRThreadView");
    NSMenu* (^messageMenu_mod)(id) = ^(id self) {
        NSMenu *menu = ((NSMenu* (*)(id, SEL))objc_msgSend)(self, @selector(mod_messageMenu));
        modifyContextMenuForThreadView(menu);
        return menu;
    };
    SWIZZLE_METHOD(object_getClass(ThreadView), messageMenu, messageMenu_mod, "@@");
    
    BOOL (^validateMenuItem_mod)(id, NSMenuItem *) = ^(id self, NSMenuItem *item) {
        if ([item action] == @selector(fireCannon:)) {
            NSURL *threadURL = [[self delegate] performSelector:@selector(threadURL)];
            if (![threadURL.host hasSuffix:@".5ch.net"] && ![threadURL.host hasSuffix:@".2ch.net"]) return NO;
            return YES;
        }
        return ((BOOL (*)(id, SEL, id))objc_msgSend)(self, @selector(mod_validateMenuItem:), item);
    };
    SWIZZLE_METHOD(ThreadView, validateMenuItem:, validateMenuItem_mod, "c@:@");

    void (^fireCannon)(id, id) = ^(id self, NSMenuItem *sender) {
        NSIndexSet *indexes = [self performSelector:@selector(messageIndexesAtClickedPoint)];
        id layout = [self performSelector:@selector(threadLayout)];
        NSArray *messages = [layout performSelector:@selector(messagesAtIndexes:) withObject:indexes];
        NSString *date = @"";
        if (messages.count) date = [messages[0] performSelector:@selector(dateRepresentation)];
        NSURL *threadURL = [[self delegate] performSelector:@selector(threadURL)];
        if ([threadURL.host hasSuffix:@".2ch.net"]) {
            NSString *newHost = [threadURL.host stringByReplacingOccurrencesOfString:@".2ch.net" withString:@".5ch.net"];
            threadURL = [[NSURL alloc] initWithScheme:threadURL.scheme host:newHost path:threadURL.path];
        }
        sender.representedObject = @[threadURL.absoluteString, date];
        [(BSTAcornIntegrator *)[BSTAcornIntegrator sharedInstance] showAcornStatusWindow:sender];
    };
    class_addMethod(ThreadView, @selector(fireCannon:), imp_implementationWithBlock(fireCannon), "v:@");
    
    for (NSMenuItem *item in [NSApp mainMenu].itemArray) {
        NSInteger insertionIndex = [item.submenu indexOfItemWithTitle:@"動作状況"];
        if (insertionIndex >= 0) {
            NSMenuItem *showAcornStatusMenu = [[NSMenuItem alloc] initWithTitle:@"どんぐり基地" action:@selector(showAcornStatusWindow:) keyEquivalent:@""];
            showAcornStatusMenu.target = self;
            [item.submenu insertItem:showAcornStatusMenu atIndex:insertionIndex+1];
            break;
        }
    }
    
    for (NSWindow *win in [NSApp orderedWindows]) {
        if ([win.windowController isKindOfClass:ThreadViewer]) {
            NSTextView *view = ((NSTextView* (*)(id, SEL))objc_msgSend)(win.windowController, @selector(textView));
            modifyContextMenuForThreadView(view.menu);
        }
    }
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"BSTLoginAcornOnLaunch"] == NSOnState) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self loginAndAlertError:NO];
        });
    }
    self.status = BSTPluginStatusEnabled;
}

- (BOOL)handleAcornCookie:(NSHTTPURLResponse *)response
{
    if (![response.URL.host hasSuffix:@".5ch.net"]) return NO;
    NSArray *cookies = [NSHTTPCookie cookiesWithResponseHeaderFields:response.allHeaderFields forURL:response.URL];
    NSMutableString *reconstructedSetCookieHeader = [NSMutableString string];
    NSString *responseHost = response.URL.host;
    for (NSHTTPCookie *cookie in cookies) {
        if ([cookie.name isEqualToString:@"acorn"]) {
            [reconstructedSetCookieHeader appendFormat:@"acorn=%@; ", cookie.value];
            if (cookie.expiresDate) {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                formatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
                formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
                formatter.dateFormat = @"EEE, dd-MMM-yyyy HH:mm:ss";
                [reconstructedSetCookieHeader appendFormat:@"expires=%@ GMT; ", [formatter stringFromDate:cookie.expiresDate]];
            }
            [reconstructedSetCookieHeader appendFormat:@"path=%@; ", cookie.path];
            NSString *domain = nil;
            if (cookie.domain) domain = cookie.domain;
            else domain = responseHost;
            if (_prefer2chDomain) {
                domain = [domain stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
                responseHost = [responseHost stringByReplacingOccurrencesOfString:@".5ch.net" withString:@".2ch.net"];
            }
            [reconstructedSetCookieHeader appendFormat:@"domain=%@", domain];
            break;
        }
    }
    if (reconstructedSetCookieHeader.length) {
        Class CookieManager = objc_lookUpClass("CookieManager");
        dispatch_async(dispatch_get_main_queue(), ^{
            id manager = [CookieManager performSelector:@selector(defaultManager)];
            [manager performSelector:@selector(addCookies:fromServer:) withObject:reconstructedSetCookieHeader withObject:responseHost];
        });
        return YES;
    }
    return NO;
}

- (NSURLRequest *)webView:(WebView *)sender
                 resource:(id)identifier
          willSendRequest:(NSURLRequest *)request
         redirectResponse:(NSURLResponse *)redirectResponse
           fromDataSource:(WebDataSource *)dataSource
{
    if ([redirectResponse isKindOfClass:[NSHTTPURLResponse class]]) {
        [self handleAcornCookie:(NSHTTPURLResponse *)redirectResponse];
    }
    return request;
}

- (void)webView:(WebView *)sender
       resource:(id)identifier
didReceiveResponse:(NSURLResponse *)response
 fromDataSource:(WebDataSource *)dataSource
{
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
        [self handleAcornCookie:(NSHTTPURLResponse *)response];
    }
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
    if ([sender.mainFrameURL isEqualToString:@"https://donguri.5ch.net/cannon"]) {
        [sender stringByEvaluatingJavaScriptFromString:@"document.querySelector('header h1').scrollIntoView()"];
        if (_cannonTargetURL) {
            NSString *script = [NSString stringWithFormat:@"document.getElementById('url').value='%@'", _cannonTargetURL];
            [sender stringByEvaluatingJavaScriptFromString:script];
        }
        if (_cannonTargetDate) {
            NSString *script = [NSString stringWithFormat:@"document.getElementById('date').value='%@'", _cannonTargetDate];
            [sender stringByEvaluatingJavaScriptFromString:script];
        }
    } else if ([sender.mainFrameURL hasPrefix:@"https://donguri.5ch.net/confirm?"]) {
        [sender stringByEvaluatingJavaScriptFromString:@"document.querySelector('header h1').scrollIntoView()"];
    } else if ([sender.mainFrameURL isEqualToString:@"https://donguri.5ch.net/"]) {
        NSString *script = @"var timer = setTimeout(function() {location.reload();}, 60*30*1000);";
        [sender stringByEvaluatingJavaScriptFromString:script];
    }
    if ([sender.mainFrameURL hasPrefix:@"https://donguri.5ch.net/"]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            NSData *data = [NSData dataWithContentsOfFile:_userScriptPath];
            if (data) {
                NSString *script = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                if (script) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [sender stringByEvaluatingJavaScriptFromString:script];
                    });
                }
            }
        });
    }
}

- (NSArray *)webView:(WebView *)sender
contextMenuItemsForElement:(NSDictionary *)element
    defaultMenuItems:(NSArray *)defaultMenuItems
{
    NSMutableArray *modifiedItems = nil;
    for (NSMenuItem *item in defaultMenuItems) {
        if (item.tag == WebMenuItemTagReload) {
            modifiedItems = [defaultMenuItems mutableCopy];
            break;
        }
    }
    if (modifiedItems) {
        if (![sender.mainFrameURL isEqualToString:@"https://donguri.5ch.net/"]) {
            NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:@"基地トップへ" action:@selector(showAcornStatusWindow:) keyEquivalent:@""];
            item.target = self;
            [modifiedItems insertObject:item atIndex:0];
        }
        NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:@"URL を開く" action:@selector(showOpenURLPanel:) keyEquivalent:@""];
        item.target = self;
        [modifiedItems addObject:item];
        return modifiedItems;
    }
    return defaultMenuItems;
}

- (WebView *)webView:(WebView *)sender createWebViewWithRequest:(NSURLRequest *)request
{
    return sender;
}

- (void)windowWillClose:(NSNotification *)notification
{
    if (notification.object == _acornStatusWindow) {
        [self.acornStatusWebView removeFromSuperview];
        self.cannonTargetURL = nil;
        self.cannonTargetDate = nil;
    }
}

- (void)showAcornStatusWindow:(id)sender
{
    Class CookieManager = objc_lookUpClass("CookieManager");
    Class AppDefaults = objc_lookUpClass("AppDefaults");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *targetURLString;
    if ([sender tag] == -1) {
        NSArray *targetInfo = [sender representedObject];
        NSString *urlParameter = @"";
        NSString *dateParameter = @"";
        if (targetInfo.count == 2) {
            urlParameter = encodeURL(targetInfo[0]);
            dateParameter = encodeURL(targetInfo[1]);
        }
        targetURLString = [NSString stringWithFormat:@"https://donguri.5ch.net/confirm?url=%@&date=%@", urlParameter, dateParameter];
        if ([defaults integerForKey:@"BSTFireCannonWithExternalBrowser"] == NSOnState) {
            BOOL inBackground = !!([NSEvent modifierFlags] & NSCommandKeyMask);
            [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:targetURLString] inBackground:inBackground];
            return;
        }
    }
    else targetURLString = @"https://donguri.5ch.net/";
    id manager = [CookieManager performSelector:@selector(defaultManager)];
    NSString *cookies = ((NSString* (*)(id, SEL, NSURL *, BOOL))objc_msgSend)(manager, @selector(cookiesForRequestURL:withBeCookie:), [NSURL URLWithString:@"https://donguri.5ch.net/"], NO);
    if (_bundleVersion < 1057 && !cookies.length) {
        cookies = ((NSString* (*)(id, SEL, NSURL *, BOOL))objc_msgSend)(manager, @selector(cookiesForRequestURL:withBeCookie:), [NSURL URLWithString:@"https://donguri.2ch.net/"], NO);
        if (cookies.length) _prefer2chDomain = YES;
    }
    if (cookies.length) {
        NSScanner *scanner = [NSScanner scannerWithString:cookies];
        [scanner scanUpToString:@"acorn=" intoString:NULL];
        if (!scanner.isAtEnd) {
            NSString *acornCookieValue = nil;
            scanner.scanLocation += 6;
            [scanner scanUpToString:@"; " intoString:&acornCookieValue];
            if (acornCookieValue.length) {
                NSHTTPCookie *acornCookie = [NSHTTPCookie cookieWithProperties:@{
                    NSHTTPCookieDomain: @".5ch.net",
                    NSHTTPCookiePath: @"/",
                    NSHTTPCookieName: @"acorn",
                    NSHTTPCookieValue: acornCookieValue
                }];
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:acornCookie];
            }
        }
    }
    id appDefaults = [AppDefaults performSelector:@selector(sharedInstance)];
    if (((BOOL (*)(id, SEL))objc_msgSend)(appDefaults, @selector(shouldLoginIfNeeded))) {
        Class Authenticator = objc_lookUpClass("w2chAuthenticator");
        NSString *sid =  [[Authenticator performSelector:@selector(defaultAuthenticator)] performSelector:@selector(sessionID)];
        if (sid.length) {
            NSHTTPCookie *sidCookie = [NSHTTPCookie cookieWithProperties:@{
                NSHTTPCookieDomain: @".5ch.net",
                NSHTTPCookiePath: @"/",
                NSHTTPCookieName: @"sid",
                NSHTTPCookieValue: sid
            }];
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:sidCookie];
        }
    }
    [self.acornStatusWebView removeFromSuperview];
    WebView *webView = [[WebView alloc] initWithFrame:_acornStatusWindow.contentView.frame frameName:nil groupName:nil];
    webView.resourceLoadDelegate = self;
    webView.frameLoadDelegate = self;
    webView.UIDelegate = self;
    if ([NSBundle respondsToSelector:@selector(monazillaUserAgentForPosting)])
        webView.customUserAgent = [NSBundle performSelector:@selector(monazillaUserAgentForPosting)];
    else {
        if (((BST5chEnabler *)[BST5chEnabler sharedInstance]).isSupplementalLoaded && [defaults integerForKey:@"BSTPretendAsNewVersion"] == NSOnState) {
            webView.customUserAgent = [defaults stringForKey:@"BSTUserAgentForNewAPI"];
        }
        else webView.customUserAgent = [NSBundle performSelector:@selector(monazillaUserAgent)];
    }
    webView.mainFrameURL = targetURLString;
    webView.autoresizingMask = NSViewWidthSizable|NSViewHeightSizable;
    [_acornStatusWindow.contentView addSubview:webView];
    [_acornStatusWindow makeKeyAndOrderFront:nil];
    self.acornStatusWebView = webView;
}

- (void)showOpenURLPanel:(id)sender
{
    NSPanel *panel = [[NSPanel alloc] initWithContentRect:NSMakeRect(0, 0, 600, 44) styleMask:NSWindowStyleMaskTitled backing:NSBackingStoreBuffered defer:NO];
    NSRect frame = panel.contentView.frame;
    frame.origin.x += 10;
    frame.origin.y += 10;
    frame.size.width -= 20;
    frame.size.height -= 20;
    NSTextField *field = [[NSTextField alloc] initWithFrame:frame];
    field.placeholderString = @"URL を入力してエンターキーを押してください";
    field.target = self;
    field.action = @selector(endSheet:);
    [panel.contentView addSubview:field];
    [NSApp beginSheet:panel modalForWindow:_acornStatusWindow modalDelegate:self didEndSelector:nil contextInfo:nil];
}

- (void)endSheet:(id)sender
{
    NSString *text = [sender stringValue];
    [[sender window] close];
    [NSApp endSheet:[sender window] returnCode:0];
    if ([text hasPrefix:@"https://donguri.5ch.net/"]) {
        self.acornStatusWebView.mainFrameURL = text;
    }
}

- (NSArray *)getAccount
{
    CFTypeRef result;
    OSStatus err = SecItemCopyMatching((__bridge CFDictionaryRef)@{
        (id)kSecClass: (id)kSecClassGenericPassword,
        (id)kSecAttrService: BSTKeychainServiceName,
        (id)kSecReturnAttributes: @YES,
        (id)kSecReturnData: @YES,
    }, &result);
    if (!err) {
        NSDictionary *dic = (__bridge_transfer NSDictionary *)result;
        NSString *user = dic[@"acct"];
        NSString *pass = [[NSString alloc] initWithData:dic[@"v_Data"] encoding:NSUTF8StringEncoding];
        return @[user ? user : @"", pass ? pass : @""];
    }
    return @[@"", @""];
}

- (NSError *)setAccountName:(NSString *)name password:(NSString *)pass
{
    if (!name || !pass || [name isEqualToString:@""] || [pass isEqualToString:@""]) {
        return [NSError errorWithDomain:@"jp.anonymous.BSTweaker" code:-1 userInfo:@{
            NSLocalizedDescriptionKey: @"メールアドレスもしくはパスワードが空です。",
        }];
    }
    OSStatus err = SecItemUpdate((__bridge CFDictionaryRef)@{
        (id)kSecClass: (id)kSecClassGenericPassword,
        (id)kSecAttrService: BSTKeychainServiceName,
    }, (__bridge CFDictionaryRef)@{
        (id)kSecAttrAccount: name,
        (id)kSecValueData: [pass dataUsingEncoding:NSUTF8StringEncoding],
    });
    if (err == errSecItemNotFound) {
        err = SecItemAdd((__bridge CFDictionaryRef)@{
            (id)kSecClass: (id)kSecClassGenericPassword,
            (id)kSecAttrService: BSTKeychainServiceName,
            (id)kSecAttrLabel: @"BSTweaker Donguri Login",
            (id)kSecAttrAccount: name,
            (id)kSecValueData: [pass dataUsingEncoding:NSUTF8StringEncoding],
        }, NULL);
    }
    if (err) {
        return [NSError errorWithDomain:NSOSStatusErrorDomain code:err userInfo:nil];
    }
    return nil;
}

- (void)loginAndAlertError:(BOOL)alertError
{
    NSArray *account = [self getAccount];
    if ([account[0] isEqualToString:@""] || [account[1] isEqualToString:@""]) return;
    self.lastLoginTrial = time(NULL);
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://donguri.5ch.net/login"] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10];
    req.HTTPMethod = @"POST";
    req.HTTPShouldHandleCookies = NO;
    [req setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSData *postData = [[NSString stringWithFormat:@"email=%@&pass=%@", encodeURL(account[0]), encodeURL(account[1])] dataUsingEncoding:NSASCIIStringEncoding];
    [req setValue:[NSString stringWithFormat:@"%ld", postData.length] forHTTPHeaderField:@"Content-Length"];
    req.HTTPBody = postData;
    BSTConnectionHandler *handler = [[BSTConnectionHandler alloc] init];
    __block BOOL stop = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [NSURLConnection connectionWithRequest:req delegate:handler];
        while (!stop) [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
    });
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_semaphore_wait(handler.semaphore, DISPATCH_TIME_FOREVER);
        stop = YES;
        NSError *error = handler.error;
        if (!error) {
            NSInteger statusCode = [(NSHTTPURLResponse *)handler.response statusCode];
            if (![self handleAcornCookie:(NSHTTPURLResponse *)handler.response]) {
                NSDictionary *headers = [(NSHTTPURLResponse *)handler.response allHeaderFields];
                NSString *informativeText = [NSString stringWithFormat:@"サーバの応答は %ld です。", statusCode];
                if ([headers[@"Content-Type"] isEqualToString:@"text/plain; charset=utf-8"]) {
                    informativeText = [[NSString alloc] initWithData:handler.data encoding:NSUTF8StringEncoding];
                }
                error = [NSError errorWithDomain:@"jp.anonymous.BSTweaker" code:-1 userInfo:@{
                    NSLocalizedDescriptionKey: @"どんぐりシステムにログインできませんでした。",
                    NSLocalizedRecoverySuggestionErrorKey: informativeText
                }];
            } else {
                self.loginRetry = 0;
                for (NSHTTPCookie *cookie in handler.cookies) {
                    if ([cookie.name isEqualToString:@"acorn"]) {
                        [NSHTTPCookieStorage.sharedHTTPCookieStorage setCookie:cookie];
                        break;
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSUserNotificationCenter *nc = [NSUserNotificationCenter defaultUserNotificationCenter];
                    nc.delegate = self;
                    NSUserNotification* notification = [[NSUserNotification alloc] init];
                    notification.title = @"BSTweaker";
                    notification.informativeText = @"どんぐりシステムにログインしました";
                    notification.hasActionButton = NO;
                    notification.soundName =  NSUserNotificationDefaultSoundName;
                    [nc deliverNotification:notification];
                });
                if (self.loginObserver) {
                    [NSNotificationCenter.defaultCenter removeObserver:self.loginObserver name:@"ThreadTextDownloaderDidFinishLoadingNotification" object:nil];
                }
                dispatch_after(dispatch_walltime(DISPATCH_TIME_NOW, 60*60*12 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    self.loginObserver = [NSNotificationCenter.defaultCenter addObserverForName:@"ThreadTextDownloaderDidFinishLoadingNotification" object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification *notification) {
                        if ([[NSUserDefaults standardUserDefaults] integerForKey:@"BSTLoginAcornOnLaunch"] != NSOnState) {
                            [NSNotificationCenter.defaultCenter removeObserver:self.loginObserver name:@"ThreadTextDownloaderDidFinishLoadingNotification" object:nil];
                            return;
                        }
                        if (time(NULL) - self.lastLoginTrial > 60) {
                            [self loginAndAlertError:NO];
                        }
                    }];
                });
            }
        }
        if (error) {
            if (alertError) dispatch_async(dispatch_get_main_queue(), ^{
                [[NSAlert alertWithError:error] runModal];
            });
            else if (self.loginRetry < 3) {
                self.loginRetry += 1;
            } else {
                [NSNotificationCenter.defaultCenter removeObserver:self.loginObserver name:@"ThreadTextDownloaderDidFinishLoadingNotification" object:nil];
                self.loginObserver = nil;
                self.loginRetry = 0;
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSUserNotificationCenter *nc = [NSUserNotificationCenter defaultUserNotificationCenter];
                    nc.delegate = self;
                    NSUserNotification* notification = [[NSUserNotification alloc] init];
                    notification.title = @"BSTweaker";
                    notification.informativeText = @"どんぐりシステムのログインに一定回数失敗したので、自動ログインを停止します。";
                    notification.hasActionButton = NO;
                    notification.soundName =  NSUserNotificationDefaultSoundName;
                    [nc deliverNotification:notification];
                });
            }
        }
    });
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification {
    return YES;
}

@end
