//
//  BSTAdvancedNGFilter.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/04.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <string>
#import <unordered_map>
#import <unordered_set>
#import <set>
#import "BSTPlugIns.h"

#define SIKIGUARD_EXPERIMENTAL 0

#ifdef USE_SIKIGUARD
struct SikiGuard {
    struct SpamData {
        std::unordered_map<std::string, std::set<time_t> > _spamIds;
        time_t _lastUpdated;
        std::string _lastModified;
        SpamData() : _lastUpdated() {};
        void update(NSURL *listURL, const dispatch_semaphore_t &semaphore) {
            time_t now = time(NULL);
            bool expired = false;
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
            if (_lastUpdated + 5*60 <= now) expired = true;
            dispatch_semaphore_signal(semaphore);
            if (!expired) return;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:listURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:10.0];
                NSHTTPURLResponse *resp;
                if (!_lastModified.empty()) [req setValue:[NSString stringWithUTF8String:_lastModified.c_str()] forHTTPHeaderField:@"If-Modified-Since"];
                NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:&resp error:NULL];
                if (resp.statusCode == 304) {
                    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                    _lastUpdated = time(NULL);
                    dispatch_semaphore_signal(semaphore);
                }
                else if (data && resp.statusCode == 200) {
                    NSDictionary *parsed = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    if (parsed && [parsed isKindOfClass:[NSDictionary class]]) {
                        NSString *lastModified = resp.allHeaderFields[@"Last-Modified"];
                        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
                        _spamIds.clear();
                        NSDictionary *result = parsed[@"result"];
                        for (NSString *day in result) {
                            if (day.length != 6) continue;
                            NSArray *list = result[day];
                            const char *ptr = day.UTF8String;
                            int year = (ptr[0] - '0') * 10 + (ptr[1] - '0');
                            int month = (ptr[2] - '0') * 10 + (ptr[3] - '0');
                            int mday = (ptr[4] - '0') * 10 + (ptr[5] - '0');
                            struct tm tm = {0};
                            tm.tm_year = (year + 2000) - 1900;
                            tm.tm_mon = month - 1;
                            tm.tm_mday = mday;
                            time_t dayBeginning = timegm(&tm) - 32400;
                            for (NSString *idStr in list) {
                                _spamIds[idStr.UTF8String].insert(dayBeginning);
                            }
                        }
                        if (lastModified) _lastModified = lastModified.UTF8String;
                        _lastUpdated = time(NULL);
                        dispatch_semaphore_signal(semaphore);
                    }
                }
            });
        }
        bool isSpamID(const std::string &idStr, time_t messageTimestamp) const {
            auto it = _spamIds.find(idStr);
            if (it != _spamIds.end()) {
                for (const auto &dayBeginning : it->second) {
                    time_t diff = messageTimestamp - dayBeginning;
                    if (diff >= 0 && diff < 86400) return true;
                }
            }
            return false;
        }
    };
    time_t _offsetToJST;
    std::unordered_map<std::string, SpamData> _entries;
    dispatch_semaphore_t _semaphore;
    NSDateFormatter *_httpDateFormatter;
    SikiGuard() {
        _semaphore = dispatch_semaphore_create(1);
        time_t t = time(NULL);
        struct tm lt = {0};
        localtime_r(&t, &lt);
        _offsetToJST = lt.tm_gmtoff - 32400;
        _httpDateFormatter = [[NSDateFormatter alloc] init];
        _httpDateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        _httpDateFormatter.dateFormat = @"EEE, dd MMM yyyy HH:mm:ss z";
        _httpDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        loadFromFile();
    }
    ~SikiGuard() {
        saveToFile();
#if !OS_OBJECT_USE_OBJC
        dispatch_release(_semaphore);
#endif
    }
    static std::string signatureForURL(NSURL *boardURL) {
        std::string signature;
        if ([boardURL.host hasSuffix:@".5ch.net"] || [boardURL.host hasSuffix:@".2ch.net"]) {
            signature = "5ch.net/";
        }
        else if ([boardURL.host hasSuffix:@".bbspink.com"]) {
            signature = "bbspink.com/";
        }
        else return "";
        NSArray<NSString *> *components = boardURL.pathComponents;
        if (components.count < 2) return "";
        signature += components[1].UTF8String;
        return signature;
    };
    void updateRuleForBoard(const std::string &signature) {
        NSURL *listURL = [[NSURL alloc] initWithScheme:@"https" host:@"sikiguard.net" path:[NSString stringWithFormat:@"/%s/id.json", signature.c_str()]];
        dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
        auto &boardData = _entries[signature];
        dispatch_semaphore_signal(_semaphore);
        boardData.update(listURL, _semaphore);
    };
    void updateRuleForBoard(NSURL *boardURL) {
        const auto &signature = SikiGuard::signatureForURL(boardURL);
        if (signature.empty()) return;
        updateRuleForBoard(signature);
    }
    bool isSpamID(const std::string &idStr, const std::string &signature, NSDate *date) {
        dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
        const auto &boardData = _entries[signature];
        bool ret = boardData.isSpamID(idStr, date.timeIntervalSince1970 + _offsetToJST);
        dispatch_semaphore_signal(_semaphore);
        return ret;
    };
    NSDate *lastUpdatedDateForBoard(const std::string &signature) {
        std::string lastModified;
        dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
        auto it = _entries.find(signature);
        if (it != _entries.end()) lastModified = it->second._lastModified;
        dispatch_semaphore_signal(_semaphore);
        if (lastModified.empty()) return nil;
        return [_httpDateFormatter dateFromString:[NSString stringWithUTF8String:lastModified.c_str()]];
    }
    NSDate *lastUpdatedDateForBoard(NSURL *boardURL) {
        const auto &signature = SikiGuard::signatureForURL(boardURL);
        if (signature.empty()) return nil;
        return lastUpdatedDateForBoard(signature);
    }
    NSString *lastUpdatedStatusStringForBoard(const std::string &signature) {
        NSDate *date = lastUpdatedDateForBoard(signature);
        if (!date) return nil;
        double diff = -[date timeIntervalSinceNow];
        if (diff < 60) return [NSString stringWithFormat:@"最終更新: %d 秒前", (int)diff];
        if (diff < 60*60) return [NSString stringWithFormat:@"最終更新: %d 分前", (int)(diff/60)];
        if (diff < 60*60*24) return [NSString stringWithFormat:@"最終更新: %d 時間前", (int)(diff/3600)];
        else return [NSString stringWithFormat:@"最終更新: %d 日前", (int)(diff/86400)];
    }
    NSString *lastUpdatedStatusStringForBoard(NSURL *boardURL) {
        const auto &signature = SikiGuard::signatureForURL(boardURL);
        if (signature.empty()) return nil;
        return lastUpdatedStatusStringForBoard(signature);
    }
    void loadFromFile() {
        NSString *path = [[[[[NSBundle bundleForClass:[BSTAdvancedNGFilter class]] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"BSTweaker-SikiGuard.plist"];
        NSDictionary *plistDic = [[NSDictionary alloc] initWithContentsOfFile:path];
        if(!plistDic) return;
        for (NSString *signature in plistDic) {
            NSDictionary *spamData = plistDic[signature];
            NSDictionary<NSString*, id> *spamIDs = spamData[@"spamIDs"];
            SpamData data;
            for (NSString *idStr in spamIDs) {
                id days = spamIDs[idStr];
                std::set<time_t> daysSet;
                if ([days isKindOfClass:[NSNumber class]])
                    daysSet.insert([days integerValue]);
                else if ([days isKindOfClass:[NSArray class]]) {
                    for (NSNumber *day in days) {
                        daysSet.insert(day.integerValue);
                    }
                }
                if (!daysSet.empty()) data._spamIds.emplace(idStr.UTF8String, std::move(daysSet));
            }
            NSString *lastModified = spamData[@"lastModified"];
            if (lastModified) data._lastModified = lastModified.UTF8String;
            _entries[signature.UTF8String] = std::move(data);
        }
    }
    void saveToFile() {
        NSString *path = [[[[[NSBundle bundleForClass:[BSTAdvancedNGFilter class]] bundlePath] stringByDeletingLastPathComponent] stringByDeletingLastPathComponent] stringByAppendingPathComponent:@"BSTweaker-SikiGuard.plist"];
        NSMutableDictionary *plistDic = [NSMutableDictionary dictionary];
        for (const auto &entry : _entries) {
            if (entry.second._spamIds.empty()) continue;
            NSMutableDictionary *spamData = [NSMutableDictionary dictionary];
            NSMutableDictionary *spamIDs = [NSMutableDictionary dictionary];
            for (const auto &data : entry.second._spamIds) {
                NSMutableArray *days = [NSMutableArray array];
                NSString *idStr = [NSString stringWithUTF8String:data.first.c_str()];
                if (!idStr) continue;
                for (const auto &dayBeginning : data.second) {
                    [days addObject:@(dayBeginning)];
                }
                spamIDs[idStr] = days;
            }
            spamData[@"lastModified"] = [NSString stringWithUTF8String:entry.second._lastModified.c_str()];
            spamData[@"spamIDs"] = spamIDs;
            NSString *signature = [NSString stringWithUTF8String:entry.first.c_str()];
            plistDic[signature] = spamData;
        }
        [plistDic writeToFile:path atomically:YES];
    }
};

static SikiGuard sg;
#if SIKIGUARD_EXPERIMENTAL
static std::unordered_set<NSUInteger> banIDWithSikiGuardForMessages(id messageBuffer, NSDictionary *threadAttributes, id boardManager, NSNumber **outUpdatedTimestamp=nullptr)
{
    std::unordered_set<NSUInteger> attrUpdatedSet;
    NSString *boardName = threadAttributes[@"BoardName"];
    BOOL useSikiGuard = ((BOOL (*)(id, SEL, NSString*, NSString*, BOOL))objc_msgSend)(boardManager, @selector(boolValueForKey:atBoard:defaultValue:), @"UseSikiGuard", boardName, NO);
    if (outUpdatedTimestamp) *outUpdatedTimestamp = nil;
    if (!useSikiGuard) return attrUpdatedSet;
    NSURL *boardURL = ((NSURL* (*)(id, SEL, NSString*))objc_msgSend)(boardManager, @selector(URLForBoardName:), boardName);
    NSDate *updated = sg.lastUpdatedDateForBoard(boardURL);
    if (updated) {
        NSNumber *lastChecked = objc_getAssociatedObject(messageBuffer, "SIKIGUARD_VERSION_TIMESTAMP");
        if (lastChecked) {
            NSLog(@"timestamp %@ is attached to messageBuffer", lastChecked);
            /* timestamp is attached to messageBuffer, possibly due to manual spam filtering */
            if (outUpdatedTimestamp) *outUpdatedTimestamp = lastChecked;
            objc_setAssociatedObject(messageBuffer, "SIKIGUARD_VERSION_TIMESTAMP", nil, OBJC_ASSOCIATION_RETAIN);
        } else {
            NSDictionary *extAttrs = threadAttributes[@"BSTExtAttrs"];
            lastChecked = extAttrs ? extAttrs[@"LastCheckedSikiGuardRuleRevision"] : nil;
        }
        if (lastChecked.integerValue < updated.timeIntervalSince1970) {
            const auto &signature = SikiGuard::signatureForURL(boardURL);
            if (signature.empty()) return attrUpdatedSet;
            NSArray *messages = ((NSArray* (*)(id, SEL))objc_msgSend)(messageBuffer, @selector(messages));
            for (id message in messages) {
                BOOL isSpam = ((BOOL (*)(id, SEL))objc_msgSend)(message, @selector(isSpam));
                NSString *idStr = ((NSString* (*)(id, SEL))objc_msgSend)(message, @selector(IDString));
                NSDate *date = ((NSDate* (*)(id, SEL))objc_msgSend)(message, @selector(date));
                if (isSpam || !idStr.length || !date) continue;
                if (sg.isSpamID(idStr.UTF8String, signature, date)) {
                    ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setPostsAttributeChangedNotifications:), NO);
                    ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setSpam:), YES);
                    ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setPostsAttributeChangedNotifications:), YES);
                    attrUpdatedSet.insert(((NSUInteger (*)(id, SEL))objc_msgSend)(message, @selector(index)));
                }
            }
            if (outUpdatedTimestamp) *outUpdatedTimestamp = [NSNumber numberWithInteger:updated.timeIntervalSince1970];
        } else NSLog(@"checking was done with revision %@, skipping", lastChecked);
    }
    return attrUpdatedSet;
}

static void setBSTExtraAttributeForKey(id obj, id attr, NSString *key)
{
    NSMutableDictionary *attrDict = ((id (*)(id, SEL))objc_msgSend)(obj, @selector(dictionaryRepresentation));
    NSMutableDictionary *extAttrs = attrDict[@"BSTExtAttrs"];
    if (!extAttrs) {
        extAttrs = [[NSMutableDictionary alloc] init];
        attrDict[@"BSTExtAttrs"] = extAttrs;
    }
    else if (![extAttrs isKindOfClass:[NSMutableDictionary class]]) {
        extAttrs = [extAttrs mutableCopy];
        attrDict[@"BSTExtAttrs"] = extAttrs;
    }
    extAttrs[key] = attr;
    ((void (*)(id, SEL, BOOL))objc_msgSend)(obj, @selector(setNeedsToUpdateLogFile:), YES);
}
#endif

static NSString* const itemTitle = @"この掲示板で SikiGuard を使う";
static void buildContextMenuForWindow(id window)
{
    NSMenu *menu = [window performSelector:@selector(drawerContextualMenu)];
    if ([menu itemWithTitle:itemTitle]) return;
    [menu addItem:[NSMenuItem separatorItem]];
    NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:itemTitle action:@selector(toggleSikiGuard:) keyEquivalent:@""];
    item.target = window;
    [menu addItem:item];
    NSMenuItem *item2 = [[NSMenuItem alloc] initWithTitle:@"" action:nil keyEquivalent:@""];
    item2.hidden = YES;
    [menu addItem:item2];
}

#endif

static std::unordered_set<NSUInteger> invokeAboneChainingTemplate(id originMessage, NSArray *messages, SEL setAttrSelector, id layoutToBeUpdated=nil) {
    NSUInteger idx = ((NSUInteger (*)(id, SEL))objc_msgSend)(originMessage, @selector(index));
    std::unordered_set<NSUInteger> attrUpdatedSet {idx};
    BOOL skip = YES;
    for (id message in messages) {
        if (skip) {
            if (message == originMessage) skip = NO;
            continue;
        }
        NSIndexSet *references = ((NSIndexSet* (*)(id, SEL))objc_msgSend)(message, @selector(referencingIndexes));
        BOOL hasReferenceToUpdatedMessage = NO;
        for (const auto &idx : attrUpdatedSet) {
            if ([references containsIndex:idx]) {
                hasReferenceToUpdatedMessage = YES;
                break;
            }
        }
        if (hasReferenceToUpdatedMessage) {
            BOOL isLocalAboned = ((BOOL (*)(id, SEL))objc_msgSend)(message, @selector(isLocalAboned));
            BOOL isInvisibleAboned = ((BOOL (*)(id, SEL))objc_msgSend)(message, @selector(isInvisibleAboned));
            if (!isLocalAboned && !isInvisibleAboned) {
                NSUInteger messageIdx = ((NSUInteger (*)(id, SEL))objc_msgSend)(message, @selector(index));
                ((void (*)(id, SEL, BOOL))objc_msgSend)(message, setAttrSelector, YES);
                attrUpdatedSet.insert(messageIdx);
            }
        }
    }
    attrUpdatedSet.erase(idx);
    if (layoutToBeUpdated) {
        for (const auto &idx : attrUpdatedSet) {
            ((void (*)(id, SEL, NSUInteger))objc_msgSend)(layoutToBeUpdated, @selector(updateMessageAtIndex:), idx);
        }
    }
    return attrUpdatedSet;
}

@interface BSTAdvancedNGFilter ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, strong) id defaultBoardManager;
@end

@implementation BSTAdvancedNGFilter

+ (id)sharedInstance
{
    static BSTAdvancedNGFilter* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTAdvancedNGFilter alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
        Class BoardManager = objc_lookUpClass("BoardManager");
        _defaultBoardManager = ((id (*)(id, SEL))objc_msgSend)(BoardManager, @selector(defaultManager));
    }
    return self;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class SpamJudge = objc_lookUpClass("BSSpamJudge");
    Class Browser = objc_lookUpClass("CMRBrowser");
    Class ThreadViewer = objc_lookUpClass("CMRThreadViewer");
    Class ThreadView = objc_lookUpClass("CMRThreadView");
    Class WindowController = objc_lookUpClass("CMRStatusLineWindowController");
    if (!SpamJudge || !Browser || !ThreadViewer || !ThreadView || !WindowController) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    id (^initWithThreadSignature_mod)(id, id) = ^(id _self, id signature) {
        id obj = [_self performSelector:@selector(initWithThreadSignature_mod:) withObject:signature];
        if(obj) {
#ifdef USE_SIKIGUARD
            NSString *boardName = ((NSString* (*)(id, SEL))objc_msgSend)(signature, @selector(boardName));
            id boardManager = self.defaultBoardManager;
            BOOL useSikiGuard = ((BOOL (*)(id, SEL, NSString*, NSString*, BOOL))objc_msgSend)(boardManager, @selector(boolValueForKey:atBoard:defaultValue:), @"UseSikiGuard", boardName, NO);
            if (useSikiGuard) {
                NSURL *boardURL = ((NSURL* (*)(id, SEL, NSString*))objc_msgSend)(boardManager, @selector(URLForBoardName:), boardName);
                const auto &signature = SikiGuard::signatureForURL(boardURL);
                if (!signature.empty()) {
                    objc_setAssociatedObject(obj, "SG_BOARD_SIGNATURE", [NSString stringWithUTF8String:signature.c_str()], OBJC_ASSOCIATION_RETAIN);
#if SIKIGUARD_EXPERIMENTAL
                    NSDate *updated = sg.lastUpdatedDateForBoard(signature);
                    if (updated) objc_setAssociatedObject(obj, "SIKIGUARD_VERSION_TIMESTAMP", [NSNumber numberWithInteger:updated.timeIntervalSince1970], OBJC_ASSOCIATION_RETAIN);
#endif
                }
            }
#endif
            objc_setAssociatedObject(obj, "THREAD_SIGNATURE", signature, OBJC_ASSOCIATION_RETAIN);
        }
        return obj;
    };
    SWIZZLE_METHOD2(SpamJudge, initWithThreadSignature:, initWithThreadSignature_mod:, initWithThreadSignature_mod, "@@:@");
    
    BOOL (^isSpamWithNGExpressionsMatch_mod)(id, id) = ^(id self, id message) {
        BOOL ret = ((BOOL (*)(id, SEL, id))objc_msgSend)(self, @selector(mod_isSpamWithNGExpressionsMatch:), message);
        if(ret) return YES;
        BOOL hasBasicInfoForAdvancedFilter = NO;
        id signature;
        NSString *identifier = nil;
        NSString *sourceBody = nil;
        NSString *sourceName = nil;
        NSString *convertedName = nil;
        NSString *convertedMail = nil;
        NSString *sourceID = [message performSelector:@selector(IDString)];
        if(!sourceID) sourceID = @"";
        
        unsigned int flag = 0;
        NSArray *expressions = nil;
        if([self respondsToSelector:@selector(NGExpressions)]) {
            expressions = [self performSelector:@selector(NGExpressions)];
        } else {
            id signature = objc_getAssociatedObject(self, "THREAD_SIGNATURE");
            if(signature) expressions = [self performSelector:@selector(mergedNGExpressionsForThreadSignature:) withObject:signature];
        }
        NSMutableIndexSet *chainOrigins = objc_getAssociatedObject(self, "ABONE_CHAINING_ORIGINS");
        for(id expression in expressions) {
            BOOL shouldCheckName = ((BOOL (*)(id, SEL))objc_msgSend)(expression, @selector(checksName));
            BOOL shouldCheckMail = ((BOOL (*)(id, SEL))objc_msgSend)(expression, @selector(checksMail));
            BOOL shouldCheckMessage = ((BOOL (*)(id, SEL))objc_msgSend)(expression, @selector(checksMessage));
            if(shouldCheckName || shouldCheckMail || shouldCheckMessage) continue;
            NSString *NGExpression = [expression performSelector:@selector(ngExpression)];
            if ([NGExpression rangeOfString:@"!ext:" options:NSLiteralSearch].location != 0) {
                id regex = [expression performSelector:@selector(regex)];
                if(regex) {
                    if([regex respondsToSelector:@selector(search:)]) {
                        if([regex performSelector:@selector(search:) withObject:sourceID]) {
                            return YES;
                        }
                    }
                    else if([regex respondsToSelector:@selector(numberOfMatchesInString:options:range:)]) {
                        if(((NSUInteger (*)(id, SEL, id, NSMatchingOptions, NSRange))objc_msgSend)(regex, @selector(numberOfMatchesInString:options:range:), sourceID, 0, NSMakeRange(0, [sourceID length]))) {
                            return YES;
                        }
                    }
                }
                else {
                    if(!NGExpression) continue;
                    if ([sourceID rangeOfString:NGExpression options:NSLiteralSearch].location != NSNotFound) {
                        return YES;
                    }
                }
            }
            else {
                BOOL match = NO;
                BOOL shouldCheckID = NO;
                unsigned int option = 0;
                unsigned int mask = 0;
                const char *str = [NGExpression UTF8String];
                const char *ptr = str+strlen("!ext:");
                const char *start = ptr;
                
                if(!hasBasicInfoForAdvancedFilter) {
                    sourceBody = [message performSelector:@selector(cachedMessage)];
                    sourceName = [message performSelector:@selector(name)];
                    NSString *sourceMail = [message performSelector:@selector(mail)];
                    
                    Class CMXTextParser = objc_lookUpClass("CMXTextParser");
                    if (sourceName && [sourceName length] > 0) {
                        convertedName = [CMXTextParser performSelector:@selector(cachedMessageWithMessageSource:) withObject:sourceName];
                    } else {
                        convertedName = nil;
                    }
                    if (sourceMail) {
                        NSUInteger mailLength = [sourceMail length];
                        if (mailLength > 4) {
                            convertedMail = [CMXTextParser performSelector:@selector(stringByReplacingEntityReference:) withObject:sourceMail];
                        } else {
                            convertedMail = sourceMail;
                        }
                    } else {
                        convertedMail = nil;
                    }
                    signature = objc_getAssociatedObject(self, "THREAD_SIGNATURE");
                    if(signature) identifier = [signature performSelector:@selector(identifier)];
                    hasBasicInfoForAdvancedFilter = YES;
                }
                
                while(*ptr != ':' && *ptr != 0) ptr++;
                if(*ptr == 0) continue;
                if((ptr-start) == 1 && *start == '*') {
                    // do nothing
                }
                else if(identifier) {
                    BOOL isTarget = NO;
                    NSString *threadKeys = [[NSString alloc] initWithBytes:start length:ptr-start encoding:NSUTF8StringEncoding];
                    NSArray *keysArr = [threadKeys componentsSeparatedByString:@","];
                    for(NSString *key in keysArr) {
                        if([key isEqualToString:identifier]) {
                            isTarget = YES;
                            break;
                        }
                    }
                    if(!isTarget) continue;
                }
                ptr++;
                start = ptr;
                while(*ptr != ':' && *ptr != 0) ptr++;
                if(*ptr == 0) continue;
                if(ptr-start == 0) continue;
                if((ptr-start) == 1 && *start == '*') {
                    shouldCheckName = YES;
                    shouldCheckMail = YES;
                    shouldCheckMessage = YES;
                    shouldCheckID = YES;
                }
                else {
                    NSString *target = [[[NSString alloc] initWithBytes:start length:ptr-start encoding:NSUTF8StringEncoding] lowercaseString];
                    NSArray *targetArr = [target componentsSeparatedByString:@","];
                    for(NSString *s in targetArr) {
                        if([s isEqualToString:@"name"]) shouldCheckName = YES;
                        else if([s isEqualToString:@"mail"]) shouldCheckMail = YES;
                        else if([s isEqualToString:@"message"]) shouldCheckMessage = YES;
                        else if([s isEqualToString:@"id"]) shouldCheckID = YES;
                    }
                }
                ptr++;
                start = ptr;
                while(*ptr != ':' && *ptr != 0) ptr++;
                if(*ptr == 0) continue;
                if(ptr-start == 0) continue;
                option = (unsigned int)strtoul(start, NULL, 16);
                mask = option & 0xffff0000;
                NGExpression = [NSString stringWithUTF8String:ptr+1];
                if(!NGExpression) continue;
                
                if (option & 0x1) {
                    NSRegularExpressionOptions regexOptions = 0;
                    if(option & 0x2) regexOptions |= NSRegularExpressionCaseInsensitive;
                    NSRegularExpression *regex = objc_getAssociatedObject(expression, "ADVANCED_NGFILTER_REGEX");
                    if(!regex || ![regex.pattern isEqualToString:NGExpression] || regex.options != regexOptions) {
                        regex = [NSRegularExpression regularExpressionWithPattern:NGExpression options:regexOptions error:nil];
                        if(regex) {
                            objc_setAssociatedObject(expression, "ADVANCED_NGFILTER_REGEX", regex, OBJC_ASSOCIATION_RETAIN);
                        }
                        else continue;
                    }
                    
                    if (sourceBody && shouldCheckMessage) {
                        if ([regex rangeOfFirstMatchInString:sourceBody options:0 range:NSMakeRange(0, [sourceBody length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (shouldCheckName) {
                        NSString *target = (option & 0x4) ? sourceName : convertedName;
                        if (target && [regex rangeOfFirstMatchInString:target options:0 range:NSMakeRange(0, [target length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (convertedMail && shouldCheckMail) {
                        if ([regex rangeOfFirstMatchInString:convertedMail options:0 range:NSMakeRange(0, [convertedMail length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (sourceID && shouldCheckID) {
                        if ([regex rangeOfFirstMatchInString:sourceID options:0 range:NSMakeRange(0, [sourceID length])].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                } else {
                    NSStringCompareOptions searchOptions = NSLiteralSearch;
                    if(option & 0x2) searchOptions |= NSCaseInsensitiveSearch;
                    if (sourceBody && shouldCheckMessage) {
                        if ([sourceBody rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (shouldCheckName) {
                        NSString *target = (option & 0x4) ? sourceName : convertedName;
                        if (target && [target rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (convertedMail && shouldCheckMail) {
                        if ([convertedMail rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                    if (sourceID && shouldCheckID) {
                        if ([sourceID rangeOfString:NGExpression options:searchOptions].location != NSNotFound) {
                            match = YES;
                            goto last;
                        }
                    }
                }
            last:
                if(match) {
                    if(mask) {
                        flag |= mask;
                        if     ((flag & 0x000f0000) == 0x000f0000) ret = YES;
                        else if((flag & 0x00f00000) == 0x00f00000) ret = YES;
                        else if((flag & 0x0f000000) == 0x0f000000) ret = YES;
                        else if((flag & 0xf0000000) == 0xf0000000) ret = YES;
                    }
                    else ret = YES;
                    if (ret) {
                        if ((option & 0x8) || (option & 0x10)) {
                            BOOL isLocalAboned = ((BOOL (*)(id, SEL))objc_msgSend)(message, @selector(isLocalAboned));
                            BOOL isInvisibleAboned = ((BOOL (*)(id, SEL))objc_msgSend)(message, @selector(isInvisibleAboned));
                            if ((option & 0x8) && !isInvisibleAboned) {
                                ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setInvisibleAboned:), YES);
                            }
                            if ((option & 0x10) && !isLocalAboned) {
                                ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setLocalAboned:), YES);
                            }
                            if (!isLocalAboned && !isInvisibleAboned && chainOrigins) {
                                [chainOrigins addIndex:((NSUInteger (*)(id, SEL))objc_msgSend)(message, @selector(index))];
                            }
                        }
                        return YES;
                    }
                }
            }
        }
        return ret;
    };
    SWIZZLE_METHOD(SpamJudge, isSpamWithNGExpressionsMatch:, isSpamWithNGExpressionsMatch_mod, "c@:@");
    
    static IMP mergeComposedResult_orig;
    void (^mergeComposedResult_mod)(id, id) = ^(id _self, id operation) {
        id messageBuffer = ((id (*)(id, SEL))objc_msgSend)(operation, @selector(messageBuffer));
#if defined(USE_SIKIGUARD) && SIKIGUARD_EXPERIMENTAL
        if (![[operation className] isEqualToString:@"BSThreadComposingOperation"]) {
            NSNumber *t = objc_getAssociatedObject(messageBuffer, "SIKIGUARD_VERSION_TIMESTAMP");
            ((void (*)(id, SEL, id))mergeComposedResult_orig)(_self, @selector(mergeComposedResult:), operation);
            if (t) {
                NSLog(@"updated skiguard revision to %@", t);
                id threadAttributes = ((id (*)(id, SEL))objc_msgSend)(_self, @selector(threadAttributes));
                setBSTExtraAttributeForKey(threadAttributes, t, @"LastCheckedSikiGuardRuleRevision");
                objc_setAssociatedObject(messageBuffer, "SIKIGUARD_VERSION_TIMESTAMP", nil, OBJC_ASSOCIATION_RETAIN);
            }
            return;
        }
#endif
        __block std::unordered_set<NSUInteger> attrUpdatedSet;
        id layout = ((id (*)(id, SEL))objc_msgSend)(_self, @selector(threadLayout));
        id currentMessageBuffer = ((id (*)(id, SEL))objc_msgSend)(layout, @selector(messageBuffer));
#if defined(USE_SIKIGUARD) && SIKIGUARD_EXPERIMENTAL
        id threadAttributes = ((id (*)(id, SEL))objc_msgSend)(_self, @selector(threadAttributes));
        if ([currentMessageBuffer count]) {
            NSNumber *updated = nil;
            NSMutableDictionary *attrDict = ((id (*)(id, SEL))objc_msgSend)(threadAttributes, @selector(dictionaryRepresentation));
            auto banned = banIDWithSikiGuardForMessages(currentMessageBuffer, attrDict, self.defaultBoardManager, &updated);
            if (!banned.empty()) attrUpdatedSet.insert(banned.begin(), banned.end());
            if (updated) setBSTExtraAttributeForKey(threadAttributes, updated, @"LastCheckedSikiGuardRuleRevision");
        } else {
            NSNumber *t = objc_getAssociatedObject(messageBuffer, "SIKIGUARD_VERSION_TIMESTAMP");
            if (t) {
                NSLog(@"updated skiguard revision to %@", t);
                setBSTExtraAttributeForKey(threadAttributes, t, @"LastCheckedSikiGuardRuleRevision");
            }
        }
#endif
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults integerForKey:@"BSTUseAboneChaining"] == NSOnState) {
            NSArray *currentMessages = ((NSArray* (*)(id, SEL))objc_msgSend)(currentMessageBuffer, @selector(messages));
            NSArray *newMessages = ((NSArray* (*)(id, SEL))objc_msgSend)(messageBuffer, @selector(messages));
            NSUInteger currentMessageCount = currentMessages.count;
            NSUInteger newMessageCount = newMessages.count;
            BOOL forceLocalAbone = [defaults integerForKey:@"BSTAboneChainingMessageAttribute"] == 1;
            BOOL forceInvisibleAbone = [defaults integerForKey:@"BSTAboneChainingMessageAttribute"] == 2;
            for (id message in newMessages) {
                NSIndexSet *references = ((NSIndexSet* (*)(id, SEL))objc_msgSend)(message, @selector(referencingIndexes));
                [references enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                    id targetMessage = nil;
                    if (idx < currentMessageCount) {
                        targetMessage = currentMessages[idx];
                    } else if (idx < newMessageCount + currentMessageCount) {
                        targetMessage = newMessages[idx-currentMessageCount];
                    }
                    if (!targetMessage) return;
                    BOOL isSpam = ((BOOL (*)(id, SEL))objc_msgSend)(targetMessage, @selector(isSpam));
                    BOOL isLocalAboned = ((BOOL (*)(id, SEL))objc_msgSend)(targetMessage, @selector(isLocalAboned));
                    BOOL isInvisibleAboned = ((BOOL (*)(id, SEL))objc_msgSend)(targetMessage, @selector(isInvisibleAboned));
                    if (isSpam && !isLocalAboned && !isInvisibleAboned) return;
                    if (forceInvisibleAbone || (!forceLocalAbone && isInvisibleAboned)) {
                        ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setInvisibleAboned:), YES);
                        attrUpdatedSet.insert(((NSUInteger (*)(id, SEL))objc_msgSend)(message, @selector(index)));
                        *stop = YES;
                        return;
                    }
                    if (forceLocalAbone || isLocalAboned) {
                        ((void (*)(id, SEL, BOOL))objc_msgSend)(message, @selector(setLocalAboned:), YES);
                        attrUpdatedSet.insert(((NSUInteger (*)(id, SEL))objc_msgSend)(message, @selector(index)));
                        *stop = YES;
                        return;
                    }
                }];
            }
        }
        ((void (*)(id, SEL, id))mergeComposedResult_orig)(_self, @selector(mergeComposedResult:), operation);
        if (!attrUpdatedSet.empty()) {
            for (const auto &idx : attrUpdatedSet) {
                ((void (*)(id, SEL, NSUInteger))objc_msgSend)(layout, @selector(updateMessageAtIndex:), idx);
            }
        }
    };
    SWIZZLE_METHOD_SAFE(ThreadViewer, mergeComposedResult:, mergeComposedResult_mod, mergeComposedResult_orig);
    
    static IMP toggleMessageAttribute_orig;
    void (^toggleMessageAttribute_mod)(id, NSInteger, NSIndexSet*) = ^(id self, NSInteger flags, NSIndexSet *set) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL chainEnabled = [defaults integerForKey:@"BSTUseAboneChaining"] == NSOnState;
        NSUInteger modifierFlags = [NSEvent modifierFlags];
        if (modifierFlags & NSCommandKeyMask) chainEnabled = !chainEnabled;
        if (!chainEnabled) {
            ((void (*)(id, SEL, NSInteger, NSIndexSet*))toggleMessageAttribute_orig)(self, @selector(toggleMessageAttribute:atIndexes:), flags, set);
            return;
        }
        BOOL forceLocalAbone = [defaults integerForKey:@"BSTAboneChainingMessageAttribute"] == 1;
        BOOL forceInvisibleAbone = [defaults integerForKey:@"BSTAboneChainingMessageAttribute"] == 2;
        Class ThreadMessage = objc_lookUpClass("CMRThreadMessage");
        id layout = ((id (*)(id, SEL))objc_msgSend)(self, @selector(threadLayout));
        IMP setLocalAboned_orig = method_getImplementation(class_getInstanceMethod(ThreadMessage, @selector(setLocalAboned:undoManager:)));
        IMP setInvisibleAboned_orig = method_getImplementation(class_getInstanceMethod(ThreadMessage, @selector(setInvisibleAboned:undoManager:)));
        IMP setLocalAboned_mod = imp_implementationWithBlock(^(id self, BOOL flag, id undoManager) {
            if (flag) {
                NSArray *messages = ((id (*)(id, SEL))objc_msgSend)(layout, @selector(allMessages));
                invokeAboneChainingTemplate(self, messages, forceInvisibleAbone ? @selector(setInvisibleAboned:) : @selector(setLocalAboned:), layout);
            }
            ((void (*)(id, SEL, BOOL, id))setLocalAboned_orig)(self, @selector(setLocalAboned:undoManager:), flag, undoManager);
        });
        IMP setInvisibleAboned_mod = imp_implementationWithBlock(^(id self, BOOL flag, id undoManager) {
            if (flag) {
                NSArray *messages = ((id (*)(id, SEL))objc_msgSend)(layout, @selector(allMessages));
                invokeAboneChainingTemplate(self, messages, forceLocalAbone ? @selector(setLocalAboned:) : @selector(setInvisibleAboned:), layout);
            }
            ((void (*)(id, SEL, BOOL, id))setInvisibleAboned_orig)(self, @selector(setInvisibleAboned:undoManager:), flag, undoManager);
        });
        method_setImplementation(class_getInstanceMethod(ThreadMessage, @selector(setLocalAboned:undoManager:)), setLocalAboned_mod);
        method_setImplementation(class_getInstanceMethod(ThreadMessage, @selector(setInvisibleAboned:undoManager:)), setInvisibleAboned_mod);
        ((void (*)(id, SEL, NSInteger, NSIndexSet*))toggleMessageAttribute_orig)(self, @selector(toggleMessageAttribute:atIndexes:), flags, set);
        method_setImplementation(class_getInstanceMethod(ThreadMessage, @selector(setLocalAboned:undoManager:)), setLocalAboned_orig);
        method_setImplementation(class_getInstanceMethod(ThreadMessage, @selector(setInvisibleAboned:undoManager:)), setInvisibleAboned_orig);
        imp_removeBlock(setLocalAboned_mod);
        imp_removeBlock(setInvisibleAboned_mod);
    };
    SWIZZLE_METHOD_SAFE(ThreadView, toggleMessageAttribute:atIndexes:, toggleMessageAttribute_mod, toggleMessageAttribute_orig);
    
    static IMP judgeMessages_orig;
    void (^judgeMessages_mod)(id, id, NSIndexSet**) = ^(id _self, id buffer, NSIndexSet **indexesPtr) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSMutableIndexSet *chainOrigins = nil;
        if (indexesPtr && [defaults integerForKey:@"BSTUseAboneChaining"] == NSOnState) {
            chainOrigins = [[NSMutableIndexSet alloc] init];
            objc_setAssociatedObject(_self, "ABONE_CHAINING_ORIGINS", chainOrigins, OBJC_ASSOCIATION_RETAIN);
        }
        ((void (*)(id, SEL, id, NSIndexSet**))judgeMessages_orig)(_self, @selector(judgeMessages:spamIndexes:), buffer, indexesPtr);
        if (chainOrigins && chainOrigins.count && [*indexesPtr isKindOfClass:[NSMutableIndexSet class]]) {
            BOOL forceLocalAbone = [defaults integerForKey:@"BSTAboneChainingMessageAttribute"] == 1;
            BOOL forceInvisibleAbone = [defaults integerForKey:@"BSTAboneChainingMessageAttribute"] == 2;
            NSArray *messages = ((NSArray* (*)(id, SEL))objc_msgSend)(buffer, @selector(messages));
            __block std::unordered_set<NSUInteger> updatedSet;
            [chainOrigins enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
                id message = messages[idx];
                BOOL isInvisibleAboned = ((BOOL (*)(id, SEL))objc_msgSend)(message, @selector(isInvisibleAboned));
                SEL selector = nil;
                if (forceInvisibleAbone) selector = @selector(setInvisibleAboned:);
                else if (forceLocalAbone) selector = @selector(setLocalAboned:);
                else selector = isInvisibleAboned ? @selector(setInvisibleAboned:) : @selector(setLocalAboned:);
                const auto &updated = invokeAboneChainingTemplate(message, messages, selector);
                if (!updated.empty()) updatedSet.insert(updated.begin(), updated.end());
            }];
            for (const auto &idx : updatedSet) {
                [((NSMutableIndexSet *)*indexesPtr) addIndex:idx];
            }
        }
#if defined(USE_SIKIGUARD) && SIKIGUARD_EXPERIMENTAL
        NSNumber *t = objc_getAssociatedObject(_self, "SIKIGUARD_VERSION_TIMESTAMP");
        if (t) {
            NSLog(@"attached timestamp %@ to messagebuffer", t);
            objc_setAssociatedObject(buffer, "SIKIGUARD_VERSION_TIMESTAMP", t, OBJC_ASSOCIATION_RETAIN);
        }
#endif
    };
    SWIZZLE_METHOD_SAFE(SpamJudge, judgeMessages:spamIndexes:, judgeMessages_mod, judgeMessages_orig);
    
#ifdef USE_SIKIGUARD
#if SIKIGUARD_EXPERIMENTAL
    static IMP writeAttributes_orig;
    void (^writeAttributes_mod)(id, NSMutableDictionary*) = ^(id self, NSMutableDictionary* dict) {
        NSMutableDictionary *attrDict = ((id (*)(id, SEL))objc_msgSend)(self, @selector(dictionaryRepresentation));
        ((void (*)(id, SEL, NSDictionary*))writeAttributes_orig)(self, @selector(writeAttributes:), dict);
        NSMutableDictionary *extAttrs = attrDict[@"BSTExtAttrs"];
        if (extAttrs) {
            dict[@"BSTExtAttrs"] = extAttrs;
        }
    };
    SWIZZLE_METHOD_SAFE(objc_lookUpClass("CMRThreadAttributes"), writeAttributes:, writeAttributes_mod, writeAttributes_orig);
    
    static IMP composeWithComposer_orig;
    void (^composeWithComposer_mod)(id, id) = ^(id _self, id composer) {
        ((void (*)(id, SEL, id))composeWithComposer_orig)(_self, @selector(composeWithComposer:), composer);
        if ([_self class] != objc_lookUpClass("CMRThreadDictReader")) return;
        NSDictionary *threadAttributes = ((id (*)(id, SEL))objc_msgSend)(_self, @selector(threadAttributes));
        NSNumber *updated = nil;
        banIDWithSikiGuardForMessages(composer, threadAttributes, self.defaultBoardManager, &updated);
        if (updated) objc_setAssociatedObject(composer, "SIKIGUARD_VERSION_TIMESTAMP", updated, OBJC_ASSOCIATION_RETAIN);
    };
    SWIZZLE_METHOD_SAFE(objc_lookUpClass("CMRThreadDictReader"), composeWithComposer:, composeWithComposer_mod, composeWithComposer_orig);
#endif
    
    static IMP downloadThread_orig;
    void (^downloadThread_mod)(id, id, NSString*, NSUInteger) = ^(id _self, id signature, NSString *title, NSUInteger nextIndex) {
        NSString *boardName = ((NSString* (*)(id, SEL))objc_msgSend)(signature, @selector(boardName));
        id boardManager = self.defaultBoardManager;
        NSURL *boardURL = ((NSURL* (*)(id, SEL, NSString*))objc_msgSend)(boardManager, @selector(URLForBoardName:), boardName);
        BOOL useSikiGuard = ((BOOL (*)(id, SEL, NSString*, NSString*, BOOL))objc_msgSend)(boardManager, @selector(boolValueForKey:atBoard:defaultValue:), @"UseSikiGuard", boardName, NO);
        if (useSikiGuard) sg.updateRuleForBoard(boardURL);
        ((void (*)(id, SEL, id, NSString*, NSUInteger))downloadThread_orig)(_self, @selector(downloadThread:title:nextIndex:), signature, title, nextIndex);
    };
    SWIZZLE_METHOD_SAFE(ThreadViewer, downloadThread:title:nextIndex:, downloadThread_mod, downloadThread_orig);
    
    static IMP isSpamWithSamplesMatch_orig;
    BOOL (^isSpamWithSamplesMatch_mod)(id, id) = ^(id self, id message) {
        BOOL ret = ((BOOL (*)(id, SEL, id))isSpamWithSamplesMatch_orig)(self, @selector(isSpamWithSamplesMatch:), message);
        if (ret) return YES;
        NSString *signature = objc_getAssociatedObject(self, "SG_BOARD_SIGNATURE");
        if (signature) {
            NSString *idStr = ((NSString* (*)(id, SEL))objc_msgSend)(message, @selector(IDString));
            NSDate *date = ((NSDate* (*)(id, SEL))objc_msgSend)(message, @selector(date));
            if (idStr.length && date) ret = sg.isSpamID(idStr.UTF8String, signature.UTF8String, date);
        }
        return ret;
    };
    SWIZZLE_METHOD_SAFE(SpamJudge, isSpamWithSamplesMatch:, isSpamWithSamplesMatch_mod, isSpamWithSamplesMatch_orig);
    
    static IMP setupUIComponents_orig;
    void (^setupUIComponents_mod)(id) = ^(id self) {
        ((void (*)(id, SEL))setupUIComponents_orig)(self, @selector(setupUIComponents));
        if([self isKindOfClass:Browser]) {
            buildContextMenuForWindow(self);
        }
    };
    SWIZZLE_METHOD_SAFE(WindowController, setupUIComponents, setupUIComponents_mod, setupUIComponents_orig);
    
    static IMP validateUserInterfaceItem_orig;
    BOOL (^validateUserInterfaceItem_mod)(id, id) = ^(id _self, id item) {
        if ([item action] == @selector(toggleSikiGuard:)) {
            NSOutlineView *boardListTable = [_self performSelector:@selector(boardListTable)];
            NSInteger clickedRow = ((NSInteger (*)(id, SEL))objc_msgSend)(boardListTable, @selector(clickedRow));
            if (clickedRow != -1) {
                id rowItem = [boardListTable itemAtRow:clickedRow];
                if ([rowItem isKindOfClass:objc_lookUpClass("BoardBoardListItem")]) {
                    NSString *name = ((NSString* (*)(id, SEL))objc_msgSend)(rowItem, @selector(name));
                    NSURL *URL = ((NSURL* (*)(id, SEL))objc_msgSend)(rowItem, @selector(url));
                    if (name && URL) {
                        NSString *host = URL.host;
                        if ([host hasSuffix:@".5ch.net"] || [host hasSuffix:@".2ch.net"] || [host hasSuffix:@".bbspink.com"]) {
                            id boardManager = self.defaultBoardManager;
                            BOOL useSikiGuard = ((BOOL (*)(id, SEL, NSString*, NSString*, BOOL))objc_msgSend)(boardManager, @selector(boolValueForKey:atBoard:defaultValue:), @"UseSikiGuard", name, NO);
                            if (useSikiGuard) {
                                [item setState:NSOnState];
                                NSString *status = sg.lastUpdatedStatusStringForBoard(URL);
                                if (status) {
                                    NSMenuItem *statusItem = [[item menu] itemAtIndex:[[item menu] indexOfItem:item]+1];
                                    statusItem.title = status;
                                    statusItem.hidden = NO;
                                }
                            }
                            else {
                                [item setState:NSOffState];
                                NSMenuItem *statusItem = [[item menu] itemAtIndex:[[item menu] indexOfItem:item]+1];
                                statusItem.hidden = YES;
                            }
                            return YES;
                        }
                    }
                }
            }
            [item setState:NSOffState];
            NSMenuItem *statusItem = [[item menu] itemAtIndex:[[item menu] indexOfItem:item]+1];
            statusItem.hidden = YES;
            return NO;
        }
        return ((BOOL (*)(id, SEL, id))validateUserInterfaceItem_orig)(_self, @selector(validateUserInterfaceItem:), item);
    };
    SWIZZLE_METHOD_SAFE(Browser, validateUserInterfaceItem:, validateUserInterfaceItem_mod, validateUserInterfaceItem_orig);
    
    void (^toggleSikiGuard)(id, id) = ^(id _self, id sender) {
        NSOutlineView *boardListTable = ((NSOutlineView* (*)(id, SEL))objc_msgSend)(_self, @selector(boardListTable));
        NSInteger clickedRow = boardListTable.clickedRow;
        if(clickedRow != -1) {
            id item = [boardListTable itemAtRow:clickedRow];
            if([item isKindOfClass:objc_lookUpClass("BoardBoardListItem")]) {
                NSString *name = ((NSString* (*)(id, SEL))objc_msgSend)(item, @selector(name));
                NSURL *URL = ((NSURL* (*)(id, SEL))objc_msgSend)(item, @selector(url));
                if(name && URL) {
                    id boardManager = self.defaultBoardManager;
                    NSInteger state = [sender state];
                    if(state == NSOnState) {
                        ((void (*)(id, SEL, NSString*, NSString*))objc_msgSend)(boardManager, @selector(removeValueForKey:atBoard:), @"UseSikiGuard", name);
                    } else {
                        ((void (*)(id, SEL, BOOL, NSString*, NSString*))objc_msgSend)(boardManager, @selector(setBoolValue:forKey:atBoard:), YES, @"UseSikiGuard", name);
                    }
                }
            }
        }
    };
    ADD_METHOD(Browser, toggleSikiGuard:, toggleSikiGuard, "v:@");
    
    NSArray *curWindows = [NSApp orderedWindows];
    for (NSWindow *win in curWindows) {
        if ([win.windowController isKindOfClass:Browser]) {
            buildContextMenuForWindow(win.windowController);
        }
    }
#endif
    
    self.status = BSTPluginStatusEnabled;
}

@end
