//
//  BSTNCRTweaker.m
//  BSTweaker
//
//  Created by anonymous on 2021/11/03.
//  Copyright © 2021年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

static NSString* const itemTitle = @"この掲示板で常に数値文字参照を使う";
static void buildContextMenuForWindow(id window)
{
    NSMenu *menu = [window performSelector:@selector(drawerContextualMenu)];
    if([menu itemWithTitle:itemTitle]) return;
    [menu addItem:[NSMenuItem separatorItem]];
    NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:itemTitle action:@selector(toggleForceUseNCR:) keyEquivalent:@""];
    item.target = window;
    [menu addItem:item];
}

@interface BSTNCRTweaker ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTNCRTweaker

+ (id)sharedInstance
{
    static BSTNCRTweaker* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTNCRTweaker alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class Browser = objc_lookUpClass("CMRBrowser");
    Class BoardManager = objc_lookUpClass("BoardManager");
    Class WindowController = objc_lookUpClass("CMRStatusLineWindowController");
    if(!Browser || !BoardManager || !WindowController) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    NSArray *curWindows = [NSApp orderedWindows];
    for(NSWindow *win in curWindows) {
        if([win.windowController isKindOfClass:Browser]) {
            buildContextMenuForWindow(win.windowController);
        }
    }
    
    static IMP setupUIComponents_orig;
    void (^setupUIComponents_mod)(id) = ^(id self) {
        ((void (*)(id, SEL))setupUIComponents_orig)(self, @selector(setupUIComponents));
        if([self isKindOfClass:Browser]) {
            buildContextMenuForWindow(self);
        }
    };
    SWIZZLE_METHOD_SAFE(WindowController, setupUIComponents, setupUIComponents_mod, setupUIComponents_orig);
    
    void (^toggleForceUseNCR)(id, id) = ^(id self, id sender) {
        NSOutlineView *boardListTable = [self performSelector:@selector(boardListTable)];
        NSInteger clickedRow = ((NSInteger (*)(id, SEL))objc_msgSend)(boardListTable, @selector(clickedRow));
        if(clickedRow != -1) {
            id item = [boardListTable itemAtRow:clickedRow];
            if([item isKindOfClass:objc_lookUpClass("BoardBoardListItem")]) {
                NSString *name = ((NSString* (*)(id, SEL))objc_msgSend)(item, @selector(name));
                NSURL *URL = ((NSURL* (*)(id, SEL))objc_msgSend)(item, @selector(url));
                if(name && URL) {
                    id manager = ((id (*)(id, SEL))objc_msgSend)(BoardManager, @selector(defaultManager));
                    NSInteger state = [sender state];
                    if(state == NSOnState) {
                        ((void (*)(id, SEL, NSString*, NSString*))objc_msgSend)(manager, @selector(removeValueForKey:atBoard:), @"ForceNCR", name);
                        
                    } else {
                        ((void (*)(id, SEL, BOOL, NSString*, NSString*))objc_msgSend)(manager, @selector(setBoolValue:forKey:atBoard:), YES, @"ForceNCR", name);
                    }
                }
            }
        }
    };
    class_addMethod(Browser, @selector(toggleForceUseNCR:), imp_implementationWithBlock(toggleForceUseNCR), "v:@");
    
    static IMP validateUserInterfaceItem_orig;
    BOOL (^validateUserInterfaceItem_mod)(id, id) = ^(id self, id item) {
        if([item action] == @selector(toggleForceUseNCR:)) {
            NSOutlineView *boardListTable = [self performSelector:@selector(boardListTable)];
            NSInteger clickedRow = ((NSInteger (*)(id, SEL))objc_msgSend)(boardListTable, @selector(clickedRow));
            if(clickedRow != -1) {
                id rowItem = [boardListTable itemAtRow:clickedRow];
                if([rowItem isKindOfClass:objc_lookUpClass("BoardBoardListItem")]) {
                    NSString *name = ((NSString* (*)(id, SEL))objc_msgSend)(rowItem, @selector(name));
                    NSURL *URL = ((NSURL* (*)(id, SEL))objc_msgSend)(rowItem, @selector(url));
                    if(name && URL) {
                        //NSString *host = URL.host;
                        //if(![host hasSuffix:@".5ch.net"] && ![host hasSuffix:@".2ch.net"] && ![host hasSuffix:@".bbspink.com"]) {
                            id manager = ((id (*)(id, SEL))objc_msgSend)(BoardManager, @selector(defaultManager));
                            BOOL forceNCR = ((BOOL (*)(id, SEL, NSString*, NSString*, BOOL))objc_msgSend)(manager, @selector(boolValueForKey:atBoard: defaultValue:), @"ForceNCR", name, NO);
                            if(forceNCR) [item setState:NSOnState];
                            else [item setState:NSOffState];
                            return YES;
                        //}
                    }
                }
            }
            [item setState:NSOffState];
            return NO;
        }
        return ((BOOL (*)(id, SEL, id))validateUserInterfaceItem_orig)(self, @selector(validateUserInterfaceItem:), item);
    };
    SWIZZLE_METHOD_SAFE(Browser, validateUserInterfaceItem:, validateUserInterfaceItem_mod, validateUserInterfaceItem_orig);
    
    if([BoardManager instancesRespondToSelector:@selector(numericCharRefAvailablityMaskAtBoard:)]) {
        uint64_t (^numericCharRefAvailablityMaskAtBoard_mod)(id, NSString*) = ^(id self, NSString* board) {
            BOOL forceNCR = ((BOOL (*)(id, SEL, NSString*, NSString*, BOOL))objc_msgSend)(self, @selector(boolValueForKey:atBoard: defaultValue:), @"ForceNCR", board, NO);
            if(forceNCR) return 3ull;
            return ((uint64_t (*)(id, SEL, NSString*))objc_msgSend)(self, @selector(mod_numericCharRefAvailablityMaskAtBoard:), board);
        };
        SWIZZLE_METHOD(BoardManager, numericCharRefAvailablityMaskAtBoard:, numericCharRefAvailablityMaskAtBoard_mod, "Q@:@");
    } else {
        BOOL (^allowsCharRefAtBoard_mod)(id, NSString*) = ^(id self, NSString* board) {
            BOOL forceNCR = ((BOOL (*)(id, SEL, NSString*, NSString*, BOOL))objc_msgSend)(self, @selector(boolValueForKey:atBoard: defaultValue:), @"ForceNCR", board, NO);
            if(forceNCR) return YES;
            return ((BOOL (*)(id, SEL, NSString*))objc_msgSend)(self, @selector(mod_allowsCharRefAtBoard:), board);
        };
        SWIZZLE_METHOD(BoardManager, allowsCharRefAtBoard:, allowsCharRefAtBoard_mod, "c@:@");
    }
    
    self.status = BSTPluginStatusEnabled;
}

@end
