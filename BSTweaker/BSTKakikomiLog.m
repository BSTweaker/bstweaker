//
//  BSTKakikomiLog.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/02.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

static NSString* const BSTKakikomiLogPath = @"~/Library/Application Support/BathyScaphe/kakikomi.txt";

@interface BSTKakikomiLog ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTKakikomiLog

+ (id)sharedInstance
{
    static BSTKakikomiLog* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTKakikomiLog alloc] init];
    });
    return sharedInstance;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    Class ReplyMessenger = objc_lookUpClass("CMRReplyMessenger");
    if(!ReplyMessenger) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    void (^didFinishPosting_mod)(id) = ^(id self) {
        NSString *title = [self performSelector:@selector(threadTitleAsString)];
        NSString *name = [self performSelector:@selector(name)];
        NSString *mail = [self performSelector:@selector(mail)];
        NSString *replyMessage = [self performSelector:@selector(replyMessage)];
        NSString *url = [self performSelector:@selector(threadURLAsString)];
        NSDate *date = [self performSelector:@selector(modifiedDate)];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
        NSString *logPath = [BSTKakikomiLogPath stringByExpandingTildeInPath];
        FILE *fp = fopen([logPath UTF8String],"a");
        if(fp) {
            fprintf(fp,"--------------------------------------------\n");
            fprintf(fp,"Date   : %s\n",[[formatter stringFromDate:date] UTF8String]);
            fprintf(fp,"Subject: %s\n",[title UTF8String]);
            fprintf(fp,"URL    : %s\n",[url UTF8String]);
            fprintf(fp,"FROM   : %s\n",name?[name UTF8String]:"");
            fprintf(fp,"MAIL   : %s\n\n",mail?[mail UTF8String]:"");
            fprintf(fp,"%s\n\n",[replyMessage UTF8String]);
            fclose(fp);
        }
        
        [self performSelector:@selector(mod_didFinishPosting)];
    };
    
    SWIZZLE_METHOD(ReplyMessenger, didFinishPosting, didFinishPosting_mod, "v@");
    
    self.status = BSTPluginStatusEnabled;
}

@end
