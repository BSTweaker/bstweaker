//
//  BSTweaker.m
//  BSTweaker
//
//  Created by anonymous on 2017/12/02.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import "BSTweaker.h"
#import "BSTPlugIns.h"

#define TO_STRING(s) #s
#define LOAD_PLUGIN_IF_ENABLED(CLASS_NAME, PREF_NAME) \
{\
    id <BSTMethodSwizzling> plugin = [CLASS_NAME sharedInstance]; \
    plugins[@TO_STRING(CLASS_NAME)] = plugin; \
    if ([[defaults objectForKey:PREF_NAME] integerValue] == NSOnState) { \
        [plugin swizzle]; \
    } \
}
#define LOAD_PLUGIN(CLASS_NAME) \
{\
    id <BSTMethodSwizzling> plugin = [CLASS_NAME sharedInstance]; \
    plugins[@TO_STRING(CLASS_NAME)] = plugin; \
    [plugin swizzle]; \
}

static NSString* const statusMessages[] = {
    @"この機能は現在無効です。",
    @"この機能は現在有効です。",
    @"この機能は現在無効です。このバージョンの BathyScaphe では不要です。",
    @"この機能は現在無効です。有効化に失敗しました。",
};

static NSAttributedString *attributedFontNameString(NSFont *font)
{
    NSString *str = [NSString stringWithFormat:@"%@ %.1f",font.displayName,font.pointSize];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.alignment = NSCenterTextAlignment;
    NSDictionary *attr = [NSDictionary dictionaryWithObjectsAndKeys:[NSFont fontWithName:font.fontName size:13],NSFontAttributeName,style,NSParagraphStyleAttributeName,nil];
    return [[NSAttributedString alloc] initWithString:str attributes:attr];
}

static NSArray *systemSounds(void)
{
    static NSArray *systemSounds;
    if (!systemSounds) {
        NSMutableArray *returnArr = [[NSMutableArray alloc] init];
        NSEnumerator *librarySources = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSAllDomainsMask, YES) objectEnumerator];
        NSString *sourcePath;
        while(sourcePath = [librarySources nextObject]) {
            NSEnumerator *soundSource = [[NSFileManager defaultManager] enumeratorAtPath:[sourcePath stringByAppendingPathComponent:@"Sounds"]];
            NSString *soundFile;
            while(soundFile = [soundSource nextObject])
                if ([NSSound soundNamed:[soundFile stringByDeletingPathExtension]])
                    [returnArr addObject:[soundFile stringByDeletingPathExtension]];
        }
        systemSounds = [[NSArray alloc] initWithArray:[returnArr sortedArrayUsingSelector:@selector(compare:)]];
    }
    return systemSounds;
}

NSColor *BSTReplyNotifierBGColor;
NSColor *BSTReplyNotifierAltBGColor;

static NSMutableDictionary *plugins;

@interface BSTPluginEnabledTransformer: NSValueTransformer {}
@end
@implementation BSTPluginEnabledTransformer
+ (Class)transformedValueClass {
    return [NSNumber class];
}
+ (BOOL)allowsReverseTransformation {
    return NO;
}
- (id)transformedValue:(id)value {
    return [NSNumber numberWithBool:([value intValue] == BSTPluginStatusEnabled)];
}
@end
@interface BSTPluginStatusTextTransformer: NSValueTransformer {}
@end
@implementation BSTPluginStatusTextTransformer
+ (Class)transformedValueClass {
    return [NSString class];
}
+ (BOOL)allowsReverseTransformation {
    return NO;
}
- (id)transformedValue:(id)value {
    if ([value intValue] > 3) return nil;
    return statusMessages[[value intValue]];
}
@end

@interface BSTweaker ()
@property(nonatomic, weak) id appDefaults;
@property(nonatomic, strong) IBOutlet NSView *o_view;
@property(nonatomic, weak) IBOutlet NSTextField *o_threadHeightField;
@property(nonatomic, weak) IBOutlet NSStepper *o_threadHeightStepper;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_boardHeightPopup;
@property(nonatomic, weak) IBOutlet NSButton *o_use5chEnabler;
@property(nonatomic, weak) IBOutlet NSButton *o_useBoardListRowHeightChanger;
@property(nonatomic, weak) IBOutlet NSButton *o_useThreadListAppearanceChanger;
@property(nonatomic, weak) IBOutlet NSButton *o_useThrobberRemover;
@property(nonatomic, weak) IBOutlet NSButton *o_threadFontButton;
@property(nonatomic, weak) IBOutlet NSColorWell *o_replyBGColor;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldChangeReplyBGColor;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldSendReplyNotification;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldDisplayBoardListIcon;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldBookmarkMyReply;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldChangeReplyAltBGColor;
@property(nonatomic, weak) IBOutlet NSColorWell *o_replyAltBGColor;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_threadTitleLineBreakModePopup;
@property(nonatomic, weak) IBOutlet NSButton *o_shouldRoute2chTo5ch;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_notificationSound;
@property(nonatomic, weak) IBOutlet NSPopUpButton *o_preferredBrowser;
@property(nonatomic, weak) IBOutlet NSButton *o_showBackslashTransparently;
@property(nonatomic, weak) IBOutlet NSButton *o_pretendAsNewVersion;
@property(nonatomic, weak) IBOutlet NSTextField *o_userAgentOverride;
@property(nonatomic, weak) IBOutlet NSButton *o_disable5chAPI;
@property(nonatomic, weak) IBOutlet NSPanel *o_acornAccountPanel;
@property(nonatomic, weak) IBOutlet NSTextField *o_acornAccountUsername;
@property(nonatomic, weak) IBOutlet NSTextField *o_acornAccountPassword;
@property(nonatomic, strong) NSMutableArray *preferredBrowsers;
@property(nonatomic, copy) NSString *threadNGFilterStatusText;
@property(nonatomic, readonly) float bundleVersion;
@property(nonatomic, strong) NSTimer *bgColorChangingTimer;
@property(nonatomic, weak) NSDictionary *plugins;
@end

@implementation BSTweaker

+ (void)initialize
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults registerDefaults:@{
            @"BSRHCThreadListHeight" : @(17),
            @"BSRHCBoardListHeight" : @(2),
            @"BSTFontName" : [NSFont systemFontOfSize:12].fontName,
            @"BSTFontSize" : @(12.0),
            @"BSTThreadTitleLineBreakMode" : @(4),
            @"BSTReplyNotifierBGColor" : [NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.95 green:0.85 blue:0.85 alpha:1.0]],
            @"BSTReplyNotifierShouldChangeBGColor" : @(NSOffState),
            @"BSTReplyNotifierAltBGColor" : [NSArchiver archivedDataWithRootObject:[NSColor colorWithDeviceRed:0.8 green:0.95 blue:1.0 alpha:1.0]],
            @"BSTReplyNotifierShouldChangeAltBGColor" : @(NSOffState),
            @"BSTReplyNotifierShouldSendNotification" : @(NSOnState),
            @"BSTReplyNotifierShouldBookmarkMyReply" : @(NSOffState),
            @"BSTAPIServerTimeout" : @(10),
            @"BSTRoute2chTo5ch" : @(NSOffState),
            @"BSTNotificationSound" : @"なし",
            @"BSTPreferredBrowser" : [[[NSWorkspace sharedWorkspace] URLForApplicationToOpenURL:[NSURL URLWithString:@"http://"]] path],
            @"BSTForce5chHTTPS" : @(NSOffState),
            @"BSTConvertYenToBackslashWhenCopy" : @(NSOnState),
            @"BSTBackslashPostingMode" : @(0),
            @"BSTShowBackslashTransparently" : @(NSOffState),
            @"BSTUserAgentForNewAPI" : @"Monazilla/1.00 BathyScaphe/3.2 Mac OS X/12.0",
            @"BSTPretendAsNewVersion" : @(NSOffState),
            @"BSTDisable5chAPI" : @(NSOnState),
            @"BSTFireCannonWithExternalBrowser" : @(NSOffState),
            @"BSTLoginAcornOnLaunch" : @(NSOffState),
            @"BSTUseAboneChaining" : @(NSOffState),
            @"BSTAboneChainingMessageAttribute" : @(0),
            @"BSTUse5chEnabler" : @(NSOnState),
            @"BSTUseKakikomiLog" : @(NSOnState),
            @"BSTUseBoardListHeightChanger" : @(NSOnState),
            @"BSTUseThreadListAppearanceChanger" : @(NSOnState),
            @"BSTUseAdvancedNGFilter" : @(NSOnState),
            @"BSTUseReplyNotifier" : @(NSOnState),
            @"BSTUseThrobberRemover" : @(NSOnState),
            @"BSTUseDig5chSearcher" : @(NSOffState),
            @"BSTUseAlertSuppressor" : @(NSOffState),
            @"BSTUseYenToBackslashConverter" : @(NSOnState),
            @"BSTUseLinkCompletion" : @(NSOffState),
            @"BSTUseBrowserSelector" : @(NSOnState),
            @"BSTUseJapaneseEraConverter" : @(NSOffState),
            @"BSTUseThreadNGFilter" : @(NSOffState),
            @"BSTUseNCRTweaker" : @(NSOffState),
            @"BSTUseAcornIntegrator" : @(NSOnState),
        }];
        
        plugins = [[NSMutableDictionary alloc] init];
        LOAD_PLUGIN_IF_ENABLED(BST5chEnabler, @"BSTUse5chEnabler")
        LOAD_PLUGIN_IF_ENABLED(BSTKakikomiLog, @"BSTUseKakikomiLog")
        LOAD_PLUGIN_IF_ENABLED(BSTBoardListHeightChanger, @"BSTUseBoardListHeightChanger")
        LOAD_PLUGIN_IF_ENABLED(BSTThreadListAppearanceChanger, @"BSTUseThreadListAppearanceChanger")
        LOAD_PLUGIN_IF_ENABLED(BSTAdvancedNGFilter, @"BSTUseAdvancedNGFilter")
        LOAD_PLUGIN_IF_ENABLED(BSTReplyNotifier, @"BSTUseReplyNotifier")
        LOAD_PLUGIN_IF_ENABLED(BSTThrobberRemover, @"BSTUseThrobberRemover")
        //LOAD_PLUGIN_IF_ENABLED(BSTDig5chSearcher, @"BSTUseDig5chSearcher")
        LOAD_PLUGIN_IF_ENABLED(BSTAlertSuppressor, @"BSTUseAlertSuppressor")
        LOAD_PLUGIN_IF_ENABLED(BSTYenToBackslashConverter, @"BSTUseYenToBackslashConverter")
        LOAD_PLUGIN_IF_ENABLED(BSTLinkCompletion, @"BSTUseLinkCompletion")
        LOAD_PLUGIN_IF_ENABLED(BSTBrowserSelector, @"BSTUseBrowserSelector")
        LOAD_PLUGIN_IF_ENABLED(BSTJapaneseEraConverter, @"BSTUseJapaneseEraConverter")
        LOAD_PLUGIN_IF_ENABLED(BSTThreadListNGFilter, @"BSTUseThreadNGFilter")
        LOAD_PLUGIN_IF_ENABLED(BSTNCRTweaker, @"BSTUseNCRTweaker")
        LOAD_PLUGIN_IF_ENABLED(BSTAcornIntegrator, @"BSTUseAcornIntegrator")
        LOAD_PLUGIN(BSTHotfix)
        if ([BSTReplyNotifier sharedInstance].status == BSTPluginStatusEnabled) {
            if([defaults integerForKey:@"BSTReplyNotifierShouldChangeBGColor"] == NSOnState) {
                NSData *colorData = [[NSUserDefaults standardUserDefaults] dataForKey:@"BSTReplyNotifierBGColor"];
                if(colorData) BSTReplyNotifierBGColor = (NSColor *)[NSUnarchiver unarchiveObjectWithData:colorData];
            }
            if([defaults integerForKey:@"BSTReplyNotifierShouldChangeAltBGColor"] == NSOnState) {
                NSData *colorData = [[NSUserDefaults standardUserDefaults] dataForKey:@"BSTReplyNotifierAltBGColor"];
                if(colorData) BSTReplyNotifierAltBGColor = (NSColor *)[NSUnarchiver unarchiveObjectWithData:colorData];
            }
        }
        [NSValueTransformer setValueTransformer:[BSTPluginEnabledTransformer new] forName:@"BSTPluginEnabledTransformer"];
        [NSValueTransformer setValueTransformer:[BSTPluginStatusTextTransformer new] forName:@"BSTPluginStatusTextTransformer"];
    });
}

- (BOOL)registerAppForPath:(NSString *)path
{
    NSBundle *appBundle = [NSBundle bundleWithPath:path];
    if(!appBundle) return NO;
    NSString *appName = [[appBundle infoDictionary] objectForKey:@"CFBundleDisplayName"];
    if(!appName) appName = [[appBundle infoDictionary] objectForKey:@"CFBundleName"];
    if(!appName) return NO;
    NSMenuItem *dupItem = [_o_preferredBrowser itemWithTitle:appName];
    if(dupItem) {
        appName = [NSString stringWithFormat:@"%@ (%@)", appName, [[appBundle infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
        dupItem = [_o_preferredBrowser itemWithTitle:appName];
        int i;
        NSString *baseAppName = appName;
        for(i=1;dupItem != nil;i++) {
            appName = [NSString stringWithFormat:@"%@ (%d)", baseAppName, i];
            dupItem = [_o_preferredBrowser itemWithTitle:appName];
        }
    }
    [_o_preferredBrowser insertItemWithTitle:appName atIndex:_preferredBrowsers.count];
    NSMenuItem *item = [_o_preferredBrowser itemAtIndex:_preferredBrowsers.count];
    NSImage *icon = [[NSWorkspace sharedWorkspace] iconForFile:path];
    [icon setSize:NSMakeSize(16, 16)];
    [item setImage:icon];
    [_preferredBrowsers addObject:path];
    return YES;
}

- (id)initWithPreferences:(AppDefaults *)prefs
{
    self = [self init];
    if(self) {
        _plugins = plugins;
        _bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [NSBundle loadNibNamed:@"Prefs" owner:self];
        [_o_threadHeightField setIntValue:[_o_threadHeightStepper intValue]];
        [_o_notificationSound addItemsWithTitles:systemSounds()];
        [_o_notificationSound selectItemWithTitle:[defaults stringForKey:@"BSTNotificationSound"]];
        _preferredBrowsers = [[NSMutableArray alloc] init];
        NSArray *handlers = (__bridge_transfer NSArray *) LSCopyAllHandlersForURLScheme((__bridge CFStringRef) @"https");
        for(NSString *bundle in handlers) {
            NSString *path = [[NSWorkspace sharedWorkspace] absolutePathForAppBundleWithIdentifier:bundle];
            if(path && [path isEqualToString:[@"/Applications" stringByAppendingPathComponent:[path lastPathComponent]]]) {
                [self registerAppForPath:path];
            }
        }
        NSString *defaultBrowserPath = [[[NSWorkspace sharedWorkspace] URLForApplicationToOpenURL:[NSURL URLWithString:@"http://"]] path];
        if(![_preferredBrowsers containsObject:defaultBrowserPath]) {
            [self registerAppForPath:defaultBrowserPath];
        }
        NSString *selectedBrowserPath = [defaults objectForKey:@"BSTPreferredBrowser"];
        if(![_preferredBrowsers containsObject:selectedBrowserPath]) {
            if(![self registerAppForPath:selectedBrowserPath]) selectedBrowserPath = defaultBrowserPath;
        }
        [_o_preferredBrowser selectItemAtIndex:[_preferredBrowsers indexOfObject:selectedBrowserPath]];
        [[_o_preferredBrowser menu] addItem:[NSMenuItem separatorItem]];
        [_o_preferredBrowser addItemWithTitle:@"その他..."];
        
        _appDefaults = prefs;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:@"BSTThreadListNGFilterRuleUpdated" object:nil];
        if(_bundleVersion < 1088) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:NSWindowWillCloseNotification object:nil];
        } else {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:NSWindowDidEndSheetNotification object:nil];
        }
        if ([BST5chEnabler sharedInstance].status == BSTPluginStatusEnabled) {
            if (_bundleVersion >= 1012 && _bundleVersion < 1101) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    if (((BOOL (*)(id, SEL))objc_msgSend)(prefs, @selector(shouldAutoSyncBoardListImmediately))) {
                        [[NSApp delegate] performSelector:@selector(runBoardWarrior:) withObject:nil];
                    }
                });
            }
        }
    }
    return self;
}

- (BOOL)previewLink:(NSURL *)url
{
    return NO;
}

- (BOOL)validateLink:(NSURL *)url
{
    return NO;
}

- (NSString *)identifierString
{
    NSBundle *bundle = [NSBundle bundleWithIdentifier:@"jp.anonymous.BSTweaker"];
    return [NSString stringWithFormat:@"BSTweaker (%@/%@)",[[bundle infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[bundle infoDictionary] objectForKey:@"CFBundleVersion"]];
}

- (NSView *)preferenceView
{
    if(!_threadNGFilterStatusText) self.threadNGFilterStatusText = statusMessages[[BSTThreadListNGFilter sharedInstance].status];
    
    if([BST5chEnabler sharedInstance].status == BSTPluginStatusBlockedByVersion) {
        _o_use5chEnabler.enabled = NO;
    }
    else if(!((BST5chEnabler *)[BST5chEnabler sharedInstance]).isSupplementalLoaded) {
        _o_pretendAsNewVersion.enabled = NO;
        _o_userAgentOverride.enabled = NO;
    }
    /*else if(bundleVersion > 1057) {
        _o_shouldRoute2chTo5ch.enabled = NO;
    }*/
    if([BSTBoardListHeightChanger sharedInstance].status == BSTPluginStatusBlockedByVersion) {
        _o_useBoardListRowHeightChanger.enabled = NO;
    }
    else if([BSTBoardListHeightChanger sharedInstance].status == BSTPluginStatusEnabled) {
        if([_appDefaults respondsToSelector:@selector(setBoardListShowsIcon:)] && [_appDefaults respondsToSelector:@selector(boardListShowsIcon)]) {
            _o_shouldDisplayBoardListIcon.state = [_appDefaults performSelector:@selector(boardListShowsIcon)] ? NSOnState : NSOffState;
        }
        else _o_shouldDisplayBoardListIcon.enabled = NO;
    }
    if([BSTThreadListAppearanceChanger sharedInstance].status == BSTPluginStatusBlockedByVersion) {
        _o_useThreadListAppearanceChanger.enabled = NO;
    }
    else if([BSTThreadListAppearanceChanger sharedInstance].status == BSTPluginStatusEnabled) {
        float shortVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        if(_bundleVersion < 1057 || shortVersion < 3.0f) {
            _o_threadHeightStepper.enabled = NO;
            _o_threadHeightField.enabled = NO;
            _o_threadFontButton.enabled = NO;
        }
    }
    if([BSTReplyNotifier sharedInstance].status == BSTPluginStatusEnabled) {
        if(_o_shouldChangeReplyBGColor.state == NSOnState) _o_replyBGColor.enabled = YES;
        else _o_replyBGColor.enabled = NO;
        if(_o_shouldChangeReplyAltBGColor.state == NSOnState) _o_replyAltBGColor.enabled = YES;
        else _o_replyAltBGColor.enabled = NO;
        if(_o_shouldSendReplyNotification.state == NSOnState) _o_notificationSound.enabled = YES;
        else _o_notificationSound.enabled = NO;
    }
    if([BSTThrobberRemover sharedInstance].status != BSTPluginStatusEnabled) {
        if([BSTThrobberRemover sharedInstance].status == BSTPluginStatusBlockedByVersion) {
            _o_useThrobberRemover.enabled = NO;
        }
    }
    if([BSTYenToBackslashConverter sharedInstance].status == BSTPluginStatusEnabled && _bundleVersion < 1012) {
        _o_showBackslashTransparently.enabled = NO;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSFont *font = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
    if(!font) font = [NSFont systemFontOfSize:[defaults floatForKey:@"BSTFontSize"]];
    _o_threadFontButton.attributedTitle = attributedFontNameString(font);
    return _o_view;
}

- (IBAction)valueChanged:(id)sender
{
    if(sender == _o_threadHeightStepper) {
        [_o_threadHeightField setStringValue:[sender stringValue]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
    }
    else if(sender == _o_boardHeightPopup || sender == _o_threadTitleLineBreakModePopup) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
    }
    else if(sender == _o_replyBGColor || sender == _o_replyAltBGColor) {
        if(sender == _o_replyBGColor) BSTReplyNotifierBGColor = [_o_replyBGColor color];
        else BSTReplyNotifierAltBGColor = [_o_replyAltBGColor color];
        [_bgColorChangingTimer invalidate];
        _bgColorChangingTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 repeats:NO block:^(NSTimer *timer) {
            [timer invalidate];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsThreadViewThemeDidChangeNotification" object:_appDefaults];
            self.bgColorChangingTimer = nil;
        }];
    }
    else if(sender == _o_shouldChangeReplyBGColor) {
        if([sender state] == NSOnState) {
            _o_replyBGColor.enabled = YES;
            BSTReplyNotifierBGColor = [_o_replyBGColor color];
        }
        else {
            _o_replyBGColor.enabled = NO;
            BSTReplyNotifierBGColor = nil;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsThreadViewThemeDidChangeNotification" object:_appDefaults];
    }
    else if(sender == _o_shouldChangeReplyAltBGColor) {
        if([sender state] == NSOnState) {
            _o_replyAltBGColor.enabled = YES;
            BSTReplyNotifierAltBGColor = [_o_replyAltBGColor color];
        }
        else {
            _o_replyAltBGColor.enabled = NO;
            BSTReplyNotifierAltBGColor = nil;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsThreadViewThemeDidChangeNotification" object:_appDefaults];
    }
    else if(sender == _o_shouldDisplayBoardListIcon) {
         ((void (*)(id, SEL, BOOL))objc_msgSend)(_appDefaults, @selector(setBoardListShowsIcon:), [sender state] == NSOnState);
    }
    else if(sender == _o_shouldSendReplyNotification) {
        if([sender state] == NSOnState) _o_notificationSound.enabled = YES;
        else _o_notificationSound.enabled = NO;
    }
    else if(sender == _o_notificationSound) {
        if([sender indexOfSelectedItem] != 0) {
            [[NSSound soundNamed:[[sender selectedItem] title]] play];
        }
    }
    else if(sender == _o_preferredBrowser) {
        NSUInteger selected = [_o_preferredBrowser indexOfSelectedItem];
        if(selected < _preferredBrowsers.count) {
            [[NSUserDefaults standardUserDefaults] setObject:_preferredBrowsers[selected] forKey:@"BSTPreferredBrowser"];
        } else {
            NSArray *appsDirs = NSSearchPathForDirectoriesInDomains(NSApplicationDirectory, NSLocalDomainMask, YES);
            NSString *appsDir = nil;
            if([appsDirs count]) appsDir = [appsDirs objectAtIndex:0];
            NSOpenPanel *openPanel = [NSOpenPanel openPanel];
            [openPanel setAllowsMultipleSelection:NO];
            [openPanel setCanChooseDirectories:NO];
            [openPanel setAllowedFileTypes:[NSArray arrayWithObject:NSFileTypeForHFSTypeCode('APPL')]];
            [openPanel setDirectoryURL:[NSURL fileURLWithPath:appsDir]];
            if([openPanel runModal] == NSOKButton) {
                NSString *selectedPath = openPanel.URL.path;
                BOOL success = YES;
                if(![_preferredBrowsers containsObject:selectedPath]) {
                    success = [self registerAppForPath:selectedPath];
                }
                if(success) [[NSUserDefaults standardUserDefaults] setObject:selectedPath forKey:@"BSTPreferredBrowser"];
            }
            [_o_preferredBrowser selectItemAtIndex:[_preferredBrowsers indexOfObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"BSTPreferredBrowser"]]];
        }
    }
    else if(sender == _o_pretendAsNewVersion) {
        if([sender state] == NSOnState) _o_userAgentOverride.enabled = YES;
        else _o_userAgentOverride.enabled = NO;
    }
    else if(sender == _o_disable5chAPI) {
        if([sender state] == NSOffState) {
            Class JIMSidManager = objc_lookUpClass("JIMSidManager");
            id manager = [JIMSidManager performSelector:@selector(sharedInstance)];
            [manager performSelector:@selector(invalidate)];
        }
    }
}

- (IBAction)selectFont:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSUInteger modifierFlags = [NSEvent modifierFlags];
    if(modifierFlags & NSCommandKeyMask) {
        NSFont *font = [NSFont systemFontOfSize:[defaults floatForKey:@"BSTFontSize"]];
        [defaults setValue:font.fontName forKey:@"BSTFontName"];
        _o_threadFontButton.attributedTitle = attributedFontNameString(font);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
    }
    else {
        NSFontManager *fontManager = [NSFontManager sharedFontManager];
        [fontManager setDelegate:self];
        [fontManager setTarget:self];
        NSFontPanel *fp = [NSFontPanel sharedFontPanel];
        NSFont *font = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
        [fp setPanelFont:font isMultiple:NO];
        [fp makeKeyAndOrderFront:nil];
    }
}

- (IBAction)setAcornPassword:(id)sender
{
    NSArray *account = [(BSTAcornIntegrator *)[BSTAcornIntegrator sharedInstance] getAccount];
    _o_acornAccountUsername.stringValue = account[0];
    _o_acornAccountPassword.stringValue = account[1];
    [_o_acornAccountUsername selectText:nil];
    NSModalResponse resp = [NSApp runModalForWindow:_o_acornAccountPanel];
    if (resp == NSModalResponseOK) {
        NSError *err = [(BSTAcornIntegrator *)[BSTAcornIntegrator sharedInstance] setAccountName:_o_acornAccountUsername.stringValue password:_o_acornAccountPassword.stringValue];
        if (err) {
            [[NSAlert alertWithError:err] runModal];
        } else {
            [[NSUserDefaults standardUserDefaults] setInteger:NSOnState forKey:@"BSTLoginAcornOnLaunch"];
        }
    }
}

- (IBAction)stopModal:(id)sender
{
    [NSApp stopModalWithCode:[sender tag]];
    [[sender window] orderOut:self];
}

- (NSUInteger)validModesForFontPanel:(NSFontPanel *)fontPanel
{
    return NSFontPanelFaceModeMask|NSFontPanelCollectionModeMask;
}

- (void)changeFont:(id)sender
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSFont *oldFont = [NSFont fontWithName:[defaults stringForKey:@"BSTFontName"] size:[defaults floatForKey:@"BSTFontSize"]];
    NSFont *newFont = [[NSFontManager sharedFontManager] convertFont:oldFont];
    //NSLog(@"%@",newFont);
    if([newFont.fontName isEqualToString:@".SFNSText"] || [newFont.fontName isEqualToString:@".SFNSDisplay"]) {
        newFont = [NSFont systemFontOfSize:newFont.pointSize];
    }
    [[NSFontManager sharedFontManager] setSelectedFont:newFont isMultiple:NO];
    [defaults setValue:newFont.fontName forKey:@"BSTFontName"];
    [defaults setFloat:newFont.pointSize forKey:@"BSTFontSize"];
    _o_threadFontButton.attributedTitle = attributedFontNameString(newFont);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AppDefaultsLayoutSettingsUpdateNotification" object:_appDefaults];
}

- (void)didReceiveNotification:(NSNotification *)aNotification
{
    if([aNotification.name isEqualToString:@"BSTThreadListNGFilterRuleUpdated"]) {
        NSUInteger rulesCount = [aNotification.userInfo[@"RulesCount"] unsignedIntegerValue];
        self.threadNGFilterStatusText = [NSString stringWithFormat:@"%@ %lu 個のルールが登録されています。",statusMessages[[BSTThreadListNGFilter sharedInstance].status],rulesCount];
    }
    else if([aNotification.name isEqualToString:NSWindowDidEndSheetNotification]) {
        if([[aNotification.object windowController].className isEqualToString:@"PreferencesPane"]) {
            [_o_view.window makeFirstResponder:nil];
        }
    }
    else if([aNotification.name isEqualToString:NSWindowWillCloseNotification]) {
        if([[aNotification.object windowController].className isEqualToString:@"BSLPSPreferenceWindow"]) {
            [_o_view.window makeFirstResponder:nil];
        }
    }
}

@end
