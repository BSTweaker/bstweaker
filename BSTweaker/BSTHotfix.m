//
//  BSTHotfix.m
//  BSTweaker
//
//  Created by anonymous on 2018/02/17.
//  Copyright © 2018年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <sys/sysctl.h>
#import <libxml/HTMLparser.h>
#import <libxml/xpath.h>
#import "BSTPlugIns.h"

@interface BSTHotfix ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@property(nonatomic, readonly) BOOL shouldEnableZWNJRemover;
@property(nonatomic, readonly) BOOL shouldEnableNavigatorFix;
@property(nonatomic, readonly) BOOL shouldEnableHTMLParserFix;
@property(nonatomic, readonly) BOOL shouldEnableHostHandlerRaceConditionFix;
@property(nonatomic, readonly) float bundleVersion;
@property(nonatomic, readonly) float shortVersion;
@end

static NSString* const preferredSystemVersionPath = @"/System/Library/CoreServices/.SystemVersionPlatform.plist";
static NSString* const defaultSystemVersionPath = @"/System/Library/CoreServices/SystemVersion.plist";

static void fixSegmentedControlPosition(NSView *control)
{
    if (control == nil) return;
    NSRect frame = control.frame;
    NSRect superFrame = control.superview.frame;
    if (superFrame.size.width - frame.origin.x != 121) return;
    if (frame.size.width != 113) return;
    if (frame.size.height != 16) return;
    frame.origin.x -= 20;
    frame.size.width += 15;
    control.frame = frame;
}

static id replyMessengerUsed;

@implementation BSTHotfix

+ (id)sharedInstance
{
    static BSTHotfix* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTHotfix alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if(self) {
        float bundleVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] floatValue];
        float shortVersion = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] floatValue];
        NSString *systemVersionPlistPath = defaultSystemVersionPath;
        if([[NSFileManager defaultManager] fileExistsAtPath:preferredSystemVersionPath]) {
            systemVersionPlistPath = preferredSystemVersionPath;
        }
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:systemVersionPlistPath];
        NSString *version = [dict objectForKey:@"ProductVersion"];
        
        if([version hasPrefix:@"10.13"]) {
            float minor =  [version substringFromIndex:3].floatValue;
            if(minor == 13.3f) {
                size_t size = 64;
                char build[64];
                sysctlbyname("kern.osversion", build, &size, NULL, 0);
                if(!strcmp(build,"17D47") || !strcmp(build,"17D2047")) {
                    _shouldEnableZWNJRemover = YES;
                }
            }
        }
        if (bundleVersion <= 1089 && shortVersion == 3.1f) {
            /* SystemVersion.plist reports 11.0 as 10.16 when SYSTEM_VERSION_COMPAT=1 */
            if (![version hasPrefix:@"10."]) {
                _shouldEnableNavigatorFix = YES;
            }
            else {
                float minor =  [version substringFromIndex:3].floatValue;
                if (minor >= 16.f) _shouldEnableNavigatorFix = YES;
            }
        }
        if(shortVersion > 2.6f && version.floatValue >= 12.0) {
            _shouldEnableHTMLParserFix = YES;
        }
        _shouldEnableHostHandlerRaceConditionFix = YES;
        _bundleVersion = bundleVersion;
        _shortVersion = shortVersion;
    }
    return self;
}

- (void)swizzle
{
    if(_status != BSTPluginStatusDisabled) return;
    
    if (_shouldEnableHostHandlerRaceConditionFix) {
        Class HostHandler = objc_lookUpClass("CMRHostHandler");
        if ([HostHandler respondsToSelector:@selector(registeredHostHandlers)]) {
            NSArray *handlers = [HostHandler performSelector:@selector(registeredHostHandlers)];
            if (handlers.count > 0) {
                [handlers[0] performSelector:@selector(properties)];
            }
        }
    }
    
    if (_shouldEnableZWNJRemover) {
        Class ThreadListItem = objc_lookUpClass("BSThreadListItem");
        Class DatabaseManager = objc_lookUpClass("DatabaseManager");
        if(!ThreadListItem || !DatabaseManager) {
            self.status = BSTPluginStatusFailedToLoad;
            return;
        }
        
        NSString* (^threadName_mod)(id, NSMutableString *) = ^(id self, NSMutableString *string) {
            NSString *str = [self performSelector:@selector(mod_threadName)];
            str = [str stringByReplacingOccurrencesOfString:@"\u200c" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
            return str;
        };
        SWIZZLE_METHOD(ThreadListItem, threadName, threadName_mod, "@@");
        
        
        NSString* (^threadTitleFromBoardName_mod)(id, NSString*, NSString*) = ^(id self, NSString *boardName, NSString *identifier) {
            NSString *str = ((NSString* (*)(id, SEL, NSString*, NSString*))objc_msgSend)(self, @selector(mod_threadTitleFromBoardName:threadIdentifier:), boardName, identifier);
            str = [str stringByReplacingOccurrencesOfString:@"\u200c" withString:@"" options:NSLiteralSearch range:NSMakeRange(0, [str length])];
            return str;
        };
        SWIZZLE_METHOD(DatabaseManager, threadTitleFromBoardName:threadIdentifier:, threadTitleFromBoardName_mod, "@@:@:@");
        
        void (^replaceEntityReference_mod)(id) = ^(id self) {
            [self replaceOccurrencesOfString:@"&#8204;" withString:@"" options:NSLiteralSearch range:NSMakeRange(0,[self length])];
            [self performSelector:@selector(mod_replaceEntityReference)];
        };
        SWIZZLE_METHOD([NSMutableString class], replaceEntityReference, replaceEntityReference_mod, "v@");
    }
    if (_shouldEnableNavigatorFix) {
        Class WindowController = objc_lookUpClass("CMRStatusLineWindowController");
        if(!WindowController) {
            self.status = BSTPluginStatusFailedToLoad;
            return;
        }
        
        NSArray *curWindows = [NSApp orderedWindows];
        for(NSWindow *win in curWindows) {
            if([win.windowController isKindOfClass:WindowController]) {
                NSView *segmentedControl = [win.windowController performSelector:@selector(indexingNavigator)];
                fixSegmentedControlPosition(segmentedControl);
            }
        }
        
        void (^windowDidLoad_mod)(id) = ^(id self) {
            [self performSelector:@selector(mod_windowDidLoad)];
            NSView *segmentedControl = [self performSelector:@selector(indexingNavigator)];
            fixSegmentedControlPosition(segmentedControl);
        };
        SWIZZLE_METHOD(WindowController, windowDidLoad, windowDidLoad_mod, "v@");
    }
    if (_shortVersion > 2.6f) {
        Class AppDefaults = objc_lookUpClass("AppDefaults");
        /* Force load 2ch connector plugin */
        id appDefaults = [AppDefaults performSelector:@selector(sharedInstance)];
        ((id (*)(id, SEL, NSURL*, NSDictionary*))objc_msgSend)(appDefaults, @selector(w2chConnectWithURL:properties:), [NSURL URLWithString:@"http://egg.2ch.net/test/bbs.cgi"], nil);
        Class SG2chErrorHandler = objc_lookUpClass("SG2chErrorHandler");
        xmlInitParser();
        static IMP parseHTMLContents_orig;
        BOOL (^parseHTMLContents_mod)(id, NSData *, NSString **, NSString **, unsigned int) = ^(id self, NSData *htmlContents, NSString **intoTitle, NSString **intoMessage, unsigned int suggestedEncoding) {
            NSString *host = ((NSURL* (*)(id, SEL))objc_msgSend)(self, @selector(requestURL)).host;
            BOOL forceLibxml2 = !([host hasSuffix:@".2ch.net"] || [host hasSuffix:@".5ch.net"] || [host hasSuffix:@".bbspink.com"] || [host hasSuffix:@"machi.to"] || [host hasSuffix:@".shitaraba.net"]);
            BOOL ret = NO;
            if (!forceLibxml2)
                ret = ((BOOL (*)(id, SEL, NSData*, NSString**, NSString **, unsigned int))parseHTMLContents_orig)(self, @selector(parseHTMLContents:intoTitle:intoMessage:suggestedEncoding:), htmlContents, intoTitle, intoMessage, suggestedEncoding);
            if (forceLibxml2 || ((_shouldEnableHTMLParserFix && ret) &&
                                 ((intoTitle && !*intoTitle) || (intoMessage && !*intoMessage)))) {
                const char *enc = NULL;
                if (forceLibxml2) {
                    if (suggestedEncoding == kCFStringEncodingUTF8) enc = "utf-8";
                    else if (suggestedEncoding == kCFStringEncodingDOSJapanese) enc = "x-sjis";
                    else if (suggestedEncoding == kCFStringEncodingEUC_JP) enc = "euc-jp";
                }
                htmlParserCtxtPtr htmlParesr = htmlNewParserCtxt();
                htmlDocPtr doc = htmlCtxtReadMemory(htmlParesr, htmlContents.bytes, (int)htmlContents.length, NULL, enc, HTML_PARSE_RECOVER|HTML_PARSE_NOERROR|HTML_PARSE_NOWARNING);
                if(doc) {
                    ret = YES;
                    xmlXPathContextPtr xpctx = xmlXPathNewContext(doc);
                    xmlXPathObjectPtr xpobj = xmlXPathEvalExpression((xmlChar *)"/html/head/title|/html/body", xpctx);
                    xmlNodeSetPtr nodes = xpobj->nodesetval;
                    for(int i=0; i<nodes->nodeNr; i++) {
                        xmlNodePtr node = nodes->nodeTab[i];
                        if (intoTitle && (!*intoTitle || forceLibxml2) && !strcasecmp((const char *)node->name, "title")) {
                            const char *contents = (const char *)xmlNodeGetContent(node);
                            if(contents) *intoTitle = [NSString stringWithUTF8String:contents];
                        }
                        if (intoMessage && (!*intoMessage || forceLibxml2) && !strcasecmp((const char *)node->name, "body")) {
                            const char *contents = (const char *)xmlNodeGetContent(node);
                            if(contents) *intoMessage = [NSString stringWithUTF8String:contents];
                        }
                    }
                    xmlXPathFreeObject(xpobj);
                    xmlXPathFreeContext(xpctx);
                    xmlFreeDoc(doc);
                }
                htmlFreeParserCtxt(htmlParesr);
            }
            if (intoTitle) {
                const char *ptr = htmlContents.bytes;
                const char *marker = memmem(ptr, htmlContents.length, "<!-- 2ch_X:", 11);
                if (marker) {
                    marker += 11;
                    if (!strncasecmp(marker, "true", 4)) *intoTitle = @"書きこみました。";
                    else if (!strncasecmp(marker, "false", 5)) *intoTitle = @"ＥＲＲＯＲ！";
                    else if (!strncasecmp(marker, "error", 5)) *intoTitle = @"ＥＲＲＯＲ！";
                    else if (!strncasecmp(marker, "cookie", 6)) *intoTitle = @"■ 書き込み確認 ■";
                }
                else if ([*intoTitle isEqualToString:@"AUTHENTICATION REQUIRED!"]) {
                    *intoTitle = @"■ 書き込み確認 ■";
                }
                if (!*intoTitle) *intoTitle = @"";
            }
            return ret;
        };
        SWIZZLE_METHOD_SAFE(SG2chErrorHandler, parseHTMLContents:intoTitle:intoMessage:suggestedEncoding:, parseHTMLContents_mod, parseHTMLContents_orig);
        
        static IMP canInitWithURL_orig;
        BOOL (^canInitWithURL_mod)(id, NSURL*) = ^(id self, NSURL *url) {
            if ([url.path.lastPathComponent isEqualToString:@"bbs.cgi"]) return YES;
            return ((BOOL (*)(id, SEL, NSURL*))canInitWithURL_orig)(self, @selector(canInitWithURL:), url);
        };
        SWIZZLE_METHOD_SAFE(object_getClass(SG2chErrorHandler), canInitWithURL:, canInitWithURL_mod, canInitWithURL_orig);
    }

    if(_bundleVersion >= 1096) {
        Class ReplyMessenger = objc_lookUpClass("CMRReplyMessenger");
        Class CookieManager = objc_lookUpClass("CookieManager");
        /* shared hook method for 5ch BBS posting via API */
        id (^uploadTaskWithRequest_mod)(id, NSURLRequest*, NSData*, void (^)(NSData *, NSURLResponse *, NSError *)) = ^(id self, NSURLRequest *request, NSData *body, void (^completionHandler)(NSData *, NSURLResponse *, NSError *)) {
            NSString *board = [replyMessengerUsed performSelector:@selector(boardName)];
            NSString *key = [replyMessengerUsed performSelector:@selector(formItemKey)];
            void (^hookHandler)(NSData *, NSURLResponse *, NSError *) = ^(NSData *data, NSURLResponse *response, NSError *error) {
                NSHTTPURLResponse *resp = (NSHTTPURLResponse *)response;
                id res = [[resp allHeaderFields] objectForKey:@"x-Resnum"];
                if(res) {
                    NSString *identifier = [NSString stringWithFormat:@"%@/%@/%d",board,key,[res intValue]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"BSTReplyNotifierRegisterIdentifier" object:self userInfo:@{@"Identifier":identifier}];
                    });
                }
                if (_bundleVersion <= 1102) { // for safety
                    id setCookie = [[resp allHeaderFields] objectForKey:@"Set-Cookie"];
                    if (setCookie) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            id manager = [CookieManager performSelector:@selector(defaultManager)];
                            [manager performSelector:@selector(addCookies:fromServer:) withObject:setCookie withObject:response.URL.host];
                        });
                    }
                }
                completionHandler(data, response, error);
            };
            if (_bundleVersion <= 1102) { // for safety
                NSMutableURLRequest *req = [request mutableCopy];
                id manager = [CookieManager performSelector:@selector(defaultManager)];
                NSString *cookies = ((NSString* (*)(id, SEL, NSURL *, BOOL))objc_msgSend)(manager, @selector(cookiesForRequestURL:withBeCookie:), req.URL, NO);
                if (cookies.length) {
                    NSString *existingCookies = [[req allHTTPHeaderFields] objectForKey:@"Cookie"];
                    if (!existingCookies || existingCookies.length == 0) {
                        [req setValue:cookies forHTTPHeaderField:@"Cookie"];
                    } else {
                        NSMutableString *newCookies = [NSMutableString stringWithString:existingCookies];
                        if (![newCookies hasSuffix:@"; "]) [newCookies appendString:@"; "];
                        [newCookies appendString:cookies];
                        [req setValue:newCookies forHTTPHeaderField:@"Cookie"];
                    }
                }
                request = req;
            }
            
            return ((id (*)(id, SEL, NSURLRequest*, NSData*, void (^)(NSData *, NSURLResponse *, NSError *)))objc_msgSend)(self, @selector(mod_uploadTaskWithRequest:fromData:completionHandler:), request, body, hookHandler);
        };
        ADD_METHOD([NSURLSession class], mod_uploadTaskWithRequest:fromData:completionHandler:, uploadTaskWithRequest_mod, "@@:@:@:?");
        /* shared swizzling for BSTReplyNotifier and BSTYenToBackslashConverter */
        void (^startSendingMessageImplNew_mod)(id) = ^(id self) {
            replyMessengerUsed = self;
            Method replyMessageMethodMod = class_getInstanceMethod([self class], @selector(mod_replyMessage));
            Method replyMessageMethodOrig = class_getInstanceMethod([self class], @selector(replyMessage));
            Method uploadTaskMethodMod = class_getInstanceMethod([NSURLSession class], @selector(mod_uploadTaskWithRequest:fromData:completionHandler:));
            Method uploadTaskMethodOrig = class_getInstanceMethod([NSURLSession class], @selector(uploadTaskWithRequest:fromData:completionHandler:));
            if(replyMessageMethodMod && replyMessageMethodOrig) method_exchangeImplementations(replyMessageMethodOrig, replyMessageMethodMod);
            if(uploadTaskMethodMod && uploadTaskMethodOrig) method_exchangeImplementations(uploadTaskMethodMod, uploadTaskMethodOrig);
            ((void (*)(id, SEL))objc_msgSend)(self, @selector(mod_startSendingMessageImplNew));
            if(replyMessageMethodMod && replyMessageMethodOrig) method_exchangeImplementations(replyMessageMethodOrig, replyMessageMethodMod);
            if(uploadTaskMethodMod && uploadTaskMethodOrig) method_exchangeImplementations(uploadTaskMethodMod, uploadTaskMethodOrig);
            replyMessengerUsed = nil;
        };
        SWIZZLE_METHOD(ReplyMessenger, startSendingMessageImplNew, startSendingMessageImplNew_mod, "v@");
    }
    
    if (_bundleVersion >= 1101) {
        Class BoardWarrior = objc_lookUpClass("BoardWarrior");
        if ([BoardWarrior instancesRespondToSelector:@selector(startJSONWarriorTask:isDebugMode:)]) {
            BOOL (^startJSONWarriorTask_mod)(id, id, BOOL) = ^(id self, id bbsmenu, BOOL isDebug) {
                NSData *modified = bbsmenu;
                if ([bbsmenu isKindOfClass:[NSData class]]) {
                    NSMutableDictionary *obj = [NSJSONSerialization JSONObjectWithData:bbsmenu options:NSJSONReadingMutableContainers error:nil];
                    if ([obj isKindOfClass:[NSMutableDictionary class]]) {
                        NSArray *categolies = obj[@"menu_list"];
                        if ([categolies isKindOfClass:[NSArray class]]) {
                            NSMutableArray *fixedCategolies = [NSMutableArray array];
                            for (id entry in categolies) {
                                if ([entry isKindOfClass:[NSDictionary class]]) {
                                    [fixedCategolies addObject:entry];
                                }
                            }
                            obj[@"menu_list"] = fixedCategolies;
                            modified = [NSJSONSerialization dataWithJSONObject:obj options:0 error:nil];
                        }
                    }
                }
                return ((BOOL (*)(id, SEL, id, BOOL))objc_msgSend)(self, @selector(mod_startJSONWarriorTask:isDebugMode:), modified, isDebug);
            };
            SWIZZLE_METHOD(BoardWarrior, startJSONWarriorTask:isDebugMode:, startJSONWarriorTask_mod, "c@:@:c");
        }
    }
    
    { // support for cookies with SameSite or Max-Age attribute
        Class CookieManager = objc_lookUpClass("CookieManager");
        static NSRegularExpression *sameSiteRegex;
        static NSRegularExpression *maxAgeRegex;
        static IMP addCookies_orig;
        sameSiteRegex = [[NSRegularExpression alloc] initWithPattern:@"( )?SameSite=[\\w]+(;)?" options:0 error:nil];
        maxAgeRegex = [[NSRegularExpression alloc] initWithPattern:@"(?: )?(Max-Age=([\\d]+))(?:;)?" options:0 error:nil];
        NSDateFormatter *httpDateFormatter = [[NSDateFormatter alloc] init];
        httpDateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
        httpDateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        httpDateFormatter.dateFormat = @"EEE, dd-MMM-yyyy HH:mm:ss";
        void (^addCookies_mod)(id, NSString*, NSString*) = ^(id self, NSString *header, NSString *hostName) {
            NSTextCheckingResult *match = [sameSiteRegex firstMatchInString:header options:0 range:NSMakeRange(0, header.length)];
            if (match) {
                header = [header stringByReplacingCharactersInRange:[match rangeAtIndex:0] withString:@""];
            }
            match = [maxAgeRegex firstMatchInString:header options:0 range:NSMakeRange(0, header.length)];
            if (match) {
                NSString *maxAge = [header substringWithRange:[match rangeAtIndex:2]];
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:time(NULL)-1+maxAge.integerValue];
                header = [header stringByReplacingCharactersInRange:[match rangeAtIndex:1] withString:[NSString stringWithFormat:@"Expires=%@ GMT", [httpDateFormatter stringFromDate:date]]];
            }
            return ((void (*)(id, SEL, NSString*, NSString*))addCookies_orig)(self, @selector(addCookies:fromServer:), header, hostName);
        };
        SWIZZLE_METHOD_SAFE(CookieManager, addCookies:fromServer:, addCookies_mod, addCookies_orig);
    }
    
    { // fix for ff5ch.syoboi.jp parsing errors
        Class SyoboiSoulGem = objc_lookUpClass("BSSyoboiSoulGem");
        if (SyoboiSoulGem) {
            static NSRegularExpression *scriptTagRegex;
            scriptTagRegex = [[NSRegularExpression alloc] initWithPattern:@"<script>.+?</script>" options:NSRegularExpressionDotMatchesLineSeparators error:nil];
            static IMP parseHTMLSource_orig;
            NSArray *(^parseHTMLSource_mod)(id, NSString*, NSError**) = ^(id self, NSString *source, NSError **errorPtr) {
                NSArray *results = ((NSArray* (*)(id, SEL, NSString*, NSError**))parseHTMLSource_orig)(self, @selector(parseHTMLSource:error:), source, errorPtr);
                if (results.count == 0) {
                    NSRange range = [source rangeOfString:@"</li>"];
                    if (range.location != NSNotFound) {
                        source = [scriptTagRegex stringByReplacingMatchesInString:source options:0 range:NSMakeRange(0, source.length) withTemplate:@""];
                        results = ((NSArray* (*)(id, SEL, NSString*, NSError**))parseHTMLSource_orig)(self, @selector(parseHTMLSource:error:), source, errorPtr);
                    }
                }
                return results;
            };
            SWIZZLE_METHOD_SAFE(SyoboiSoulGem, parseHTMLSource:error:, parseHTMLSource_mod, parseHTMLSource_orig);
        }
    }
    
    self.status = BSTPluginStatusEnabled;
}

@end
