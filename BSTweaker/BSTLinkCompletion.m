//
//  BSTLinkCompletion.m
//  BSTweaker
//
//  Created by anonymous on 2019/01/09.
//  Copyright © 2019年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BSTPlugIns.h"

static NSRegularExpression *partialURLRegex;
static NSRegularExpression *nonCompliantURLRegex;

static void complementLinks(NSMutableAttributedString *mStr, id inlineThumbnailComposer)
{
    [mStr enumerateAttribute:NSLinkAttributeName inRange:NSMakeRange(0, mStr.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
        if (!value || ![value isKindOfClass:[NSURL class]]) return;
        if (![[value path] isEqualToString:@""]) return;
        NSString *str = mStr.string;
        NSUInteger length = str.length;
        NSInteger nextIndex = range.location+range.length;
        if (nextIndex >= length-1) return;
        unichar c = [str characterAtIndex:nextIndex];
        if (c != ' ' && c != 0x3000) return;
        NSRange match = [partialURLRegex rangeOfFirstMatchInString:str options:0 range:NSMakeRange(nextIndex+1, length-nextIndex-1)];
        if (match.location == NSNotFound) return;
        NSString *newURLStr = [NSString stringWithFormat:@"%@%@",[value absoluteString],[str substringWithRange:match]];
        NSURL *url = [NSURL URLWithString:newURLStr];
        NSRange newRange = NSMakeRange(range.location, range.length+match.length+1);
        [mStr removeAttribute:NSLinkAttributeName range:range];
        [mStr addAttribute:NSLinkAttributeName value:url range:newRange];
        if (inlineThumbnailComposer) {
            ((BOOL (*)(id, SEL, NSURL*, NSRange))objc_msgSend)(inlineThumbnailComposer, @selector(addLink:range:), url, newRange);
        }
    }];
    NSArray *matches = [nonCompliantURLRegex matchesInString:mStr.string options:0 range:NSMakeRange(0, mStr.length)];
    for (NSTextCheckingResult *match in matches) {
        __block BOOL hasAnchor = NO;
        NSRange matchRange = match.range;
        [mStr enumerateAttribute:NSLinkAttributeName inRange:matchRange options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
            if (!value) return;
            hasAnchor = YES;
            *stop = YES;
        }];
        if (hasAnchor) continue;
        if (matchRange.location != 0) {
            if ([mStr.string characterAtIndex:matchRange.location] != '/') {
                if ([mStr.string characterAtIndex:matchRange.location-1] != '\n') continue;
            }
            else if ([mStr.string characterAtIndex:matchRange.location-1] == ':') continue;
        }
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://%@", [mStr.string substringWithRange:[match rangeAtIndex:1]]]];
        if (url) {
            [mStr addAttribute:NSLinkAttributeName value:url range:matchRange];
            if (inlineThumbnailComposer) {
                ((BOOL (*)(id, SEL, NSURL*, NSRange))objc_msgSend)(inlineThumbnailComposer, @selector(addLink:range:), url, matchRange);
            }
        }
    }
}

@interface BSTLinkCompletion ()
@property(nonatomic, readwrite) BSTPluginStatus status;
@end

@implementation BSTLinkCompletion

+ (id)sharedInstance
{
    static BSTLinkCompletion* sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BSTLinkCompletion alloc] init];
        partialURLRegex = [[NSRegularExpression alloc] initWithPattern:@"^[0-9a-zA-Z.-]+/[0-9a-zA-Z.-_%&=+?!#~,@()/:;']*" options:0 error:nil];
        nonCompliantURLRegex = [[NSRegularExpression alloc] initWithPattern:@"(?:/+)?((?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}/[-a-zA-Z0-9()@:%_\\+.~#?&/=]*)" options:0 error:nil];
    });
    return sharedInstance;
}

- (void)swizzle
{
    Class AttributedMessageComposer = objc_lookUpClass("CMRAttributedMessageComposer");
    Class ThumbnailComposer = objc_lookUpClass("BSInlineThumbnailComposer");
    Class BeSAAPAnchorComposer = objc_lookUpClass("BSBeSAAPAnchorComposer");
    if(!AttributedMessageComposer || (ThumbnailComposer && !BeSAAPAnchorComposer)) {
        self.status = BSTPluginStatusFailedToLoad;
        return;
    }
    
    BOOL useOldThumbnailComposer = NO;
    if(ThumbnailComposer && [ThumbnailComposer instancesRespondToSelector:@selector(insertAllInlineThumbnailsIfNeeded:)])
        useOldThumbnailComposer = YES;
    
    if([AttributedMessageComposer instancesRespondToSelector:@selector(makeOuterLinkAnchor:)]) {
        void (^makeOuterLinkAnchor_mod)(id, NSMutableAttributedString *) = ^(id self, NSMutableAttributedString *mStr) {
            objc_setAssociatedObject(mStr, "BST_DELAY_THUMBNAIL_COMPOSING", @YES, OBJC_ASSOCIATION_RETAIN);
            ((void (*)(id, SEL, NSMutableAttributedString *))objc_msgSend)(self, @selector(mod_makeOuterLinkAnchor:), mStr);
            id inlineThumbnailComposer = nil;
            id beIconComposer = objc_getAssociatedObject(mStr, "BST_BEICON_COMPOSER");
            if(ThumbnailComposer) {
                inlineThumbnailComposer = objc_getAssociatedObject(mStr, "BST_THUMBNAIL_COMPOSER");
                if(!inlineThumbnailComposer) {
                    inlineThumbnailComposer = [[ThumbnailComposer alloc] init];
                    NSSize thumbnailPointSize = ((NSSize (*)(id, SEL))objc_msgSend)(self, @selector(inlineThumbnailPointSize));
                    NSSize thumbnailPixelSize = ((NSSize (*)(id, SEL))objc_msgSend)(self, @selector(inlineThumbnailPixelSize));
                    ((void (*)(id, SEL, NSSize))objc_msgSend)(inlineThumbnailComposer, @selector(setThumbnailPointSize:), thumbnailPointSize);
                    ((void (*)(id, SEL, NSSize))objc_msgSend)(inlineThumbnailComposer, @selector(setThumbnailPixelSize:), thumbnailPixelSize);
                }
            }
            complementLinks(mStr, inlineThumbnailComposer);
            objc_setAssociatedObject(mStr, "BST_DELAY_THUMBNAIL_COMPOSING", nil, OBJC_ASSOCIATION_RETAIN);
            if(ThumbnailComposer) {
                if(inlineThumbnailComposer && ((BOOL (*)(id, SEL))objc_msgSend)(inlineThumbnailComposer, @selector(hasLinks))) {
                    if(useOldThumbnailComposer) {
                        ((void (*)(id, SEL, id))objc_msgSend)(inlineThumbnailComposer, @selector(insertAllInlineThumbnailsIfNeeded:), mStr);
                    } else {
                        BOOL compose = ((BOOL (*)(id, SEL))objc_msgSend)(self, @selector(composeInlineThumbnail));
                        ((void (*)(id, SEL, id, BOOL))objc_msgSend)(inlineThumbnailComposer, @selector(insertAllInlineThumbnailsIfNeeded:supplyIfCacheNotExists:), mStr, compose);
                    }
                }
                objc_setAssociatedObject(mStr, "BST_THUMBNAIL_COMPOSER", nil, OBJC_ASSOCIATION_RETAIN);
            }
            if(beIconComposer) {
                ((void (*)(id, SEL, id))objc_msgSend)(beIconComposer, @selector(composeAllSAAPAnchorsIfNeeded:), mStr);
                objc_setAssociatedObject(mStr, "BST_BEICON_COMPOSER", nil, OBJC_ASSOCIATION_RETAIN);
            }
        };
        SWIZZLE_METHOD(AttributedMessageComposer, makeOuterLinkAnchor:, makeOuterLinkAnchor_mod, "v@:@");
    } else {
        void (^makeOuterLinkAnchor_mod)(id, NSMutableAttributedString *, id) = ^(id self, NSMutableAttributedString *mStr, id message) {
            ((void (*)(id, SEL, NSMutableAttributedString *, id))objc_msgSend)(self, @selector(mod_makeOuterLinkAnchor:inMessage:), mStr, message);
            static NSRegularExpression *partialURLRegex;
            if(!partialURLRegex) {
                partialURLRegex = [[NSRegularExpression alloc] initWithPattern:@"^[0-9a-zA-Z.-]+/[0-9a-zA-Z.-_%&=+?!#~,@()/:;']*" options:0 error:nil];
            }
            complementLinks(mStr, nil);
        };
        SWIZZLE_METHOD(AttributedMessageComposer, makeOuterLinkAnchor:inMessage:, makeOuterLinkAnchor_mod, "v@:@:@");
    }
    
    if(ThumbnailComposer) {
        if(useOldThumbnailComposer) {
            void (^insertAllInlineThumbnailsIfNeeded)(id, id) = ^(id self, id arg1) {
                id shouldDelay = objc_getAssociatedObject(arg1, "BST_DELAY_THUMBNAIL_COMPOSING");
                if([shouldDelay boolValue]) {
                    objc_setAssociatedObject(arg1, "BST_THUMBNAIL_COMPOSER", self, OBJC_ASSOCIATION_RETAIN);
                    return;
                }
                ((void (*)(id, SEL, id))objc_msgSend)(self, @selector(mod_insertAllInlineThumbnailsIfNeeded:), arg1);
            };
            SWIZZLE_METHOD(ThumbnailComposer, insertAllInlineThumbnailsIfNeeded:, insertAllInlineThumbnailsIfNeeded, "v@:@");
        }
        else {
            void (^insertAllInlineThumbnailsIfNeeded)(id, id, BOOL) = ^(id self, id arg1, BOOL arg2) {
                id shouldDelay = objc_getAssociatedObject(arg1, "BST_DELAY_THUMBNAIL_COMPOSING");
                if([shouldDelay boolValue]) {
                    objc_setAssociatedObject(arg1, "BST_THUMBNAIL_COMPOSER", self, OBJC_ASSOCIATION_RETAIN);
                    return;
                }
                ((void (*)(id, SEL, id, BOOL))objc_msgSend)(self, @selector(mod_insertAllInlineThumbnailsIfNeeded:supplyIfCacheNotExists:), arg1, arg2);
            };
            SWIZZLE_METHOD(ThumbnailComposer, insertAllInlineThumbnailsIfNeeded:supplyIfCacheNotExists:, insertAllInlineThumbnailsIfNeeded, "v@:@:c");
        }
        
        void (^composeAllSAAPAnchorsIfNeeded)(id, id) = ^(id self, id arg1) {
            id shouldDelay = objc_getAssociatedObject(arg1, "BST_DELAY_THUMBNAIL_COMPOSING");
            if([shouldDelay boolValue]) {
                objc_setAssociatedObject(arg1, "BST_BEICON_COMPOSER", self, OBJC_ASSOCIATION_RETAIN);
                return;
            }
            ((void (*)(id, SEL, id))objc_msgSend)(self, @selector(mod_composeAllSAAPAnchorsIfNeeded:), arg1);
        };
        SWIZZLE_METHOD(BeSAAPAnchorComposer, composeAllSAAPAnchorsIfNeeded:, composeAllSAAPAnchorsIfNeeded, "v@:@");
    }
    
    self.status = BSTPluginStatusEnabled;
}

@end
