//
//  BSTPlugIns.h
//  BSTweaker
//
//  Created by anonymous on 2017/12/04.
//  Copyright © 2017年 anonymous. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "swizzle.h"

@interface BSTKakikomiLog : NSObject <BSTMethodSwizzling>
@end

@interface BST5chEnabler : NSObject <BSTMethodSwizzling>
@property(nonatomic, readonly) BOOL isSupplementalLoaded;
@end

@interface BSTBoardListHeightChanger : NSObject <BSTMethodSwizzling>
@end

@interface BSTThreadListAppearanceChanger : NSObject <BSTMethodSwizzling>
@end

@interface BSTAdvancedNGFilter : NSObject <BSTMethodSwizzling>
@end

@interface BSTReplyNotifier : NSObject <BSTMethodSwizzling>
@end

@interface BSTThrobberRemover : NSObject <BSTMethodSwizzling>
@end

@interface BSTHotfix : NSObject <BSTMethodSwizzling>
@end

@interface BSTDig5chSearcher : NSObject <BSTMethodSwizzling>
@end

@interface BSTAlertSuppressor : NSObject <BSTMethodSwizzling>
@end

@interface BSTYenToBackslashConverter : NSObject <BSTMethodSwizzling>
@end

@interface BSTLinkCompletion : NSObject <BSTMethodSwizzling>
@end

@interface BSTBrowserSelector : NSObject <BSTMethodSwizzling>
@end

@interface BSTJapaneseEraConverter : NSObject <BSTMethodSwizzling>
@end

@interface BSTThreadListNGFilter: NSObject <BSTMethodSwizzling>
@end

@interface BSTNCRTweaker: NSObject <BSTMethodSwizzling>
@end

@interface BSTAcornIntegrator: NSObject <BSTMethodSwizzling>
- (NSArray *)getAccount;
- (NSError *)setAccountName:(NSString *)name password:(NSString *)pass;
@end
